//app.js
import wux from 'components/wux'

App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    var that = this; 
    // 将缓存中的用户微信id赋值给全局的
    wx.getStorage({
      key: 'userViewId',
      success: function (res) {
        console.log("将缓存中的用户微信id赋值给全局的userViewId:"+res.data)
        that.globalData.userViewId = res.data;
      }
    })
    // 调取微信登录，获取openId
    wx.login({
      success: function (res) {
        console.log("登陆成功");
        console.log(res);
        that.getOpenId(res.code);
      },
      fail: function(){
        console.log("登录失败");
      }
    });
  },
  //获取openid
  getOpenId: function (code) {
    var that = this;
    wx.request({
      url: that.globalData.BaseURL + '/wx/openId',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: { 'code': code},
      success: function (res1) {
        var stringResData = JSON.parse(res1.data.data);
        var openId = stringResData.openId;
        var userViewId = stringResData.userViewId;
        console.log("初始化userViewId:" + userViewId);
        that.globalData.openId = openId;
        that.globalData.userViewId = userViewId;
        //钩子函数，成功后，执行拼团分享页的数据加载
        if (that.userInfoReadyCallback) {
          that.userInfoReadyCallback(res1)
        }
        // 获取用户信息
        wx.getSetting({
          success: res => {
            if (res.authSetting['scope.userInfo']) {
              // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
              wx.getUserInfo({
                withCredentials: true,
                success: res => {
                  // 可以将 res 发送给后台解码出 unionId
                  that.globalData.userInfo = res.userInfo;
                  that.globalData.avatarUrl = res.userInfo.avatarUrl; 
                  that.globalData.nickName = res.userInfo.nickName;
                  that.globalData.hasUserInfo = true;
                  var userInfoData = JSON.stringify(res);
                  console.log("userInfoData=======" + userInfoData);
                  //获取用户信息成功，传到后台
                  wx.request({
                    url: that.globalData.BaseURL + '/wx/userinfo',
                    method: 'POST',
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded",
                      "userViewId": that.globalData.userViewId
                    },
                    data: { 'data': userInfoData },
                    success: function (res) {
                      var unionId = res.data.data;
                      that.globalData.unionId = unionId;
                    },
                    fail: function (res) {
                      wx.showToast({
                        title: '请求失败，请退出重试',
                        duration: 2500
                      })
                    }
                  })
                },
                fail: function (res) {
                  wx.showToast({
                    title: '获取信息失败，请退出重试',
                    duration: 2500
                  })
                  that.globalData.openId = "";
                }
              })
            } else {
              // that.$wuxToast.show({
              //   type: 'text',
              //   timer: 2500,
              //   color: '#fff',
              //   text: '您还未授权获取您的用户信息，请退出重试！',
              //   success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
              // })
            }
          },
          fail: function (res) {
            console.log("无法获取已向用户请求过的权限");
          }
        })
      },
      fail: function(res){
        console.log("失败");
        that.globalData.openId = "";
      }
    })
  },
  //时间戳转换时间  
  toDate: function (number) {
    var n = number;
    var date = new Date(n);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    return (Y + M + D)
  },

  getUserInfo: function (cb) {
    if (that.globalData.userInfo) {
      typeof cb == "function" && cb(this.globalData.userInfo)
    } else {
      //调用登录接口
      wx.getUserInfo({
        withCredentials: true,
        success: function (res) {
          var userInfo = res.userInfo;
          console.log('获取到的用户信息为:' + res.userInfo);
          that.globalData.userInfo = userInfo
          typeof cb == "function" && cb(that.globalData.userInfo);
          that.globalData.hasUserInfo = true;
          that.globalData.avatarUrl = res.userInfo.avatarUrl;
          that.globalData.nickName = res.userInfo.nickName;
        }
      })
    }
  },

  globalData: {
    BaseURL: "https://shop.airburgzen.cn",
    // BaseURL: "http://192.168.43.153:8080",
    openId: null,  
    userInfo: null,
    userViewId: null,  //未使用
    avatarUrl: null, //用户头像
    nickName:null,//用户姓名
    unionId: null,
    // 填写的备注信息
    remarkStr: "",
    // 填写的发票信息和纳税人信息
    fpInfo: "",
    nsrInfo: "",
    hasUserInfo: false,
    //退款备注
    refundRemark:"",
    // 选中蛋糕的spu信息
    cakeSpuItem: null,
    /* 
     *核对订单页面 
     */
    // 选中的地址信息
    addressList: "",
    // 选中的代金券信息
    couponItem: null,
    //选中的亲友码id
    relativeCodeId:null,
    //拼团的id
    groupId:null,
    ptInfoId:null,
    //区分拼团、亲友码、积分
    tap:null,
    //用户选中的商品
    chooseGoodsDtoList:null,
    //客服电话
    phoneNumber: '400-8270838', 
    //创建订单时是否选择了亲友码
    loadCouponListPage:false,
    //创建订单时选择了个人还是公司抬头
    compony: false,
  },
  // 通过scope来引入wux函数
  wux: (scope) => new wux(scope)
})
