// model.js
var dates = [];
var hours =[];
var secondhours = [];
var selectedHours = [];
var value = [0, 0];//数据位置下标

function setDateTimesValue(setDates, setHours, setSecondhours) {
  dates = setDates;
  hours = setHours;
  secondhours = setSecondhours;
}

function updateDateTimesData(that, status, e) {
  //滑动事件
  var valueChange = function (e, that) {
    var val = e.detail.value
    console.log(e)
    //判断滑动的是第几个column
    //若省份column做了滑动则定位到地级市和区县第一位
    if (val[0] == 0) {
      selectedHours = hours;
    } else {
      selectedHours = secondhours;
    }
    if (value[0] != val[0]) {
      val[1] = 0;
    }
    value = val;
    // 调用回调赋值接口
    assignmentData(that, that.data.item.show)
    console.log(val);

    //回调
    //callBack(val);
  }

  if (status == 0) {
    // 第一次进入赋值
    selectedHours = hours;
  } else {
    //滑动事件赋值
    valueChange(e, that);
  }

}

//动画事件
function animationEvents(that, moveY, show, duration) {
  console.log("moveY:" + moveY + "\nshow:" + show);
  that.animation = wx.createAnimation({
    transformOrigin: "50% 50%",
    duration: duration,
    timingFunction: "ease",
    delay: 0
  })
  that.animation.translateY(moveY + 'vh').step()
  //赋值
  assignmentData(that, show)

}

// 回调赋值
function assignmentData(that, show) {
  that.setData({
    item: {
      dates: dates,
      selectedHours: selectedHours,
      animation: that.animation.export(),
      show: show,
      value: value
    }
  })
}

module.exports = {
  updateDateTimesData: updateDateTimesData,
  setDateTimesValue: setDateTimesValue,
  animationEvents: animationEvents
}


















