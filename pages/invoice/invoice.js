const util = require("../../utils/util.js");
var app = getApp();
var that;


Page({

  /**
   * 页面的初始数据
   */
  data: {
    compony: "",
    nsrInfo:"",//纳税人识别号
    fpInfo:"",//发票抬头
  },
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
  },
  onShow: function () {
    that.setData({
      compony: app.globalData.compony
    })
    if (that.data.compony!="个人"){//公司抬头
      that.setData({
        nsrInfo: app.globalData.nsrInfo,
        fpInfo: app.globalData.fpInfo,
      })
    }
  },
  radioChange:function(e){
    var chooseType=e.detail.value;
    if (chooseType === "compony") {//公司抬头
      app.globalData.compony = true;
      that.setData({
        compony:true,
      })
      if (app.globalData.fpInfo !== "个人") {
        that.setData({
          fpInfo: app.globalData.fpInfo,
          nsrInfo: app.globalData.nsrInfo
        })
      } else {//避免读取个人时的抬头和纳税人识别号
        that.setData({
          fpInfo: "",
          nsrInfo: ""
        })
      }
    } else if (chooseType === "personal") {//个人抬头
      app.globalData.compony = false;
      that.setData({
        compony: false,
        fpInfo: "个人",
        nsrInfo: "无"
      })
    }
  },
  onGotUserInfo: function (e) {
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.invoiceTitle']) {
          wx.authorize({
            scope: 'scope.invoiceTitle',
            success() {
              // 用户已经同意小程序使用录音功能，后续调用 wx.startRecord 接口不会弹窗询问
              wx.chooseInvoiceTitle({
                success(res) {
                  console.log("1:" +JSON.stringify(res));
                  that.setData({
                    fpInfo: res.title,
                    nsrInfo: res.taxNumber
                  })
                  app.globalData.fpInfo = res.title;
                  app.globalData.nsrInfo = res.taxNumber;
                }
              })
            }
          })
        }else{
          wx.chooseInvoiceTitle({
            success(res) {
              console.log("2:"+JSON.stringify(res));
              that.setData({
                fpInfo: res.title,
                nsrInfo: res.taxNumber
              })
              app.globalData.fpInfo = res.title;
              app.globalData.nsrInfo = res.taxNumber;
            }
          })
        }
      }
    })
  },
  // 发票
  fapiaoInput: function (e) {
    that.setData({
      fpInfo: e.detail.value
    });
    app.globalData.fpInfo = e.detail.value;
    console.log("fpInfo:" + app.globalData.fpInfo);
  },
  // 纳税人识别号
  nsrsbhInput: function (e) {
    that.setData({
      nsrInfo: e.detail.value
    });
    app.globalData.nsrInfo = e.detail.value;
  },
  confirmInvoice:function(){
    if (that.data.compony == true ){
      if (that.data.fpInfo == ""){
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请填写公司名称'
        })
        return;
      } else if (that.data.nsrInfo == ""){
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请填写纳税人识别号'
        })
        return;
      }
      app.globalData.compony = true;
      app.globalData.fpInfo = that.data.fpInfo;
      app.globalData.nsrInfo = that.data.nsrInfo;
    } else {
      app.globalData.compony = false;
      app.globalData.fpInfo = "个人";
      app.globalData.nsrInfo = "无";
    }
    console.log("app.globalData.fpInfo=========" + app.globalData.fpInfo);
    console.log("app.globalData.nsrInfo=========" + app.globalData.nsrInfo);
    wx.navigateBack();
  }
})