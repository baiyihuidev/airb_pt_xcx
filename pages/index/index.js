const util = require("../../utils/util.js");
var app = getApp();
var that;

Page({
  data: {
    // 轮播图相关设置
    swiperCurrent: 0,
    visual:'hidden',
    indicatorDots: true,  // 是否显示pages
    autoplay: true,//自动播放   
    interval: 3500,//间隔时间   
    duration: 1000,//监听滚动和点击事件  
    circular: true,
    imageWidth: wx.getSystemInfoSync().windowWidth,
    bannerList: [],
    // http://static.cakeboss.com.cn/kqbBanner1.svg
    scrollTop: 0,
    // 切换tab标记（0:亲友码，1:拼团，2:积分）
    currentTabsIndex:0,
    // 亲友码商品列表
    friendGoodsList:[],
    // 拼团商品列表
    pinTuanGoodsList: [],
    // 积分兑换商品列表
    pointGoodsList: [],
    userInfo: {},
    hasUserInfo: false,
    //点击进入商品详情页的goodsId
    goodsId:'',
    //新、老用户类型
    userType:'',
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    var currentTabsIndex = options.currentTabsIndex;
    console.log(JSON.stringify(options),"options");
    console.log("首页读取的currentTabsIndex:" + options.currentTabsIndex);
    that.getBannerList();
    wx.request({//从我领取的亲友码列表--去使用按钮，点击进来和默认都显示亲友码商品列表
      url: app.globalData.BaseURL + '/relativeCode',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) { 
        that.setData({
          friendGoodsList: res.data.data
        })
      }
    })
  },
  onShareAppMessage: function (res) {
    return {
      path: '/pages/index/index',
      title:'空气堡AIRBURG，智慧新风净化系统',
      success: function (options) {
        util.shareSuccessTost();
      },
      fail: function (options) {
        util.shareCancelTost();
      }
    }
  }, 
  // 返回顶部
  goTop: function (e) {
    that.setData({
      scrollTop: 0
    })
  },
  console:function(e) {
    var index = e.currentTarget.dataset['url'];
    console.log("首页banner跳转======="+index);
  },
  // 获取banner图片数组
  getBannerList: function() {
    wx.request({
      url: app.globalData.BaseURL + '/banner/getBannerList',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log('bannerLaist请求数据为：', res);
        that.setData({
          bannerList: res.data.data
        })
        // for (i = 0; i < res.data.data.length; i++) {
        //   if (res.data.data.actionUrl==="小程序"){
        //     if (wx.navigateToMiniProgram) {
        //       wx.navigateToMiniProgram({
        //         appId: wx85f7f3eb72578a4e
        //       })
        //     } else {
        //       wx.previewImage({
        //         urls: [recommend.qrcode],
        //       })
        //     }
        //   }
        // }
      }
    })
  },
  //切换亲友码、拼团、积分商城
  onTabsItemTap:function(e){
    var index = e.currentTarget.dataset.index;
    console.log(index);
    var url;
    var dataFlag=false;
    if (index==0){
      url ="/relativeCode";
      if (that.data.friendGoodsList == null || that.data.friendGoodsList == ""){
        dataFlag = true;
        that.setData({
          visual: 'show'
        })
      }else{
        that.setData({
          visual: 'hidden'
        })
      }
    }else if(index==1){
      url = "/spu/pt/list";
      if (that.data.pinTuanGoodsList == null || that.data.pinTuanGoodsList == "") {
        dataFlag = true;
        that.setData({
          visual: 'show'
        })
      } else {
        that.setData({
          visual: 'hidden'
        })
      }
    }else{
      url = "/integral";
      if (that.data.pointGoodsList == null || that.data.pointGoodsList == "") {
        dataFlag = true;
        that.setData({
          visual: 'show'
        })
      } else {
        that.setData({
          visual: 'hidden'
        })
      }
    }
    that.setData({
      currentTabsIndex: index
    })
    if (dataFlag == true){
      wx.request({
        url: app.globalData.BaseURL + url, //仅为示例，并非真实的接口地址
        header: {
          'content-type': 'application/json' // 默认值
        },
        method: 'GET',
        success: function (res) {
          var orderList = res.data.data;
          if (orderList == null || orderList == "") {
            that.setData({
              visual: 'show'
            }) 
          } else {
            that.setData({
              visual: 'hidden'
            })
          }
          if(index==0){
            that.setData({
              friendGoodsList: res.data.data
            })
          }else if(index==1) {
            that.setData({
              pinTuanGoodsList: res.data.data
            })
          }else{
            that.setData({
              pointGoodsList: res.data
            })
          }
          console.log(res.data);
        }
      })
    }
  },
  linkToDetailPage:function(event){
    let goodsId=event.currentTarget.dataset.id;
    let userType = event.currentTarget.dataset.type;
    console.log("首页读取全局的userViewId：" + app.globalData.userViewId);
    that.setData({
      goodsId: goodsId,
      userType: userType
    })
    if (app.globalData.userViewId) {
      that.setData({
        hasUserInfo: true
      });
      console.log("判断是否有权限跳转进入详情页时发送的userViewId:" + app.globalData.userViewId);
      that.judgeLinkToDetailPageFunc();
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
      // 所以此处加入 callback 以防止这种情况  
      app.userInfoReadyCallback = res => {
        var userInfo = JSON.parse(res.data.data);
        var userViewId = userInfo.userViewId;
        that.setData({
          hasUserInfo: true
        });
        app.globalData.userViewId = userViewId;
        console.log("2判断是否有权限跳转进入详情页时发送的userViewId:" + userViewId);
        that.judgeLinkToDetailPageFunc();
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理  
      wx.getSetting({
        success: data => {
          if (data.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                var userInfo = JSON.parse(res.data.data);
                var userViewId = userInfo.userViewId;
                that.setData({
                  hasUserInfo: true
                });
                app.globalData.userViewId = userViewId;
                console.log("3判断是否有权限跳转进入详情页时发送的userViewId:" + userViewId);
                that.judgeLinkToDetailPageFunc();
              },
              fail: res => {
                that.$wuxToast.show({
                  type: 'text',
                  timer: 2500,
                  color: '#fff',
                  text: '获取信息失败，请返回首页后重试',
                  success: () => console.log('获取信息失败，请返回首页后重试')
                })
              }
            })
          } else {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '您还未授权获取您的用户信息，请退出重试！',
              success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
            })
          }
        },
        fail: function () {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: '无法获取授权信息，请退出重试',
            success: () => console.log('无法获取授权信息，请退出重试')
          })
        }
      });
    } 
  },
  judgeLinkToDetailPageFunc:function(){
    util.showLoading();
    if (that.data.userType == 4) {//新用户
      wx.navigateTo({
        url: '/pages/detail/friendCodeToBuy/detail?goodsId=' + that.data.goodsId + '&tap=4'
      })
    } else if (that.data.userType == 3) {//老用户
      wx.navigateTo({
        url: '/pages/detail/buyFriendCode/detail?goodsId=' + that.data.goodsId + '&tap=1'
      })
    } else if (that.data.userType == 5) {//拼团活动
      wx.navigateTo({
        url: '/pages/detail/groupActivity/detail?goodsId=' + that.data.goodsId + '&tap=5'
      })
    }
    // wx.request({  
    //   url: app.globalData.BaseURL + '/relativeCode/detail?id=' + that.data.goodsId,
    //   method: 'GET',
    //   header: {
    //     "Content-Type": "application/x-www-form-urlencoded",
    //     "userViewId": app.globalData.userViewId
    //   },
    //   success: function (res) {
    //     util.hideToast();
    //     var code = res.data.code;
    //     console.log(that.data.userType,"that.data.userType");
    //     if (code == 200) {
    //       if (that.data.userType == 4) {//新用户
    //         wx.navigateTo({
    //           url: '/pages/detail/friendCodeToBuy/detail?goodsId=' + that.data.goodsId + '&tap=4'
    //         })
    //       } else if (that.data.userType == 3) {//老用户
    //         wx.navigateTo({
    //           url: '/pages/detail/buyFriendCode/detail?goodsId=' + that.data.goodsId + '&tap=1'
    //         })
    //       } else if (that.data.userType == 5) {//拼团活动
    //         wx.navigateTo({
    //           url: '/pages/detail/groupActivity/detail?goodsId=' + that.data.goodsId + '&tap=5'
    //         })
    //       }
    //     } else {
    //       util.hideToast();
    //       that.$wuxToast.show({
    //         type: 'text',
    //         timer: 1500,
    //         color: '#fff',
    //         text: res.data.msg,
    //         success: () => console.log(res.data.msg)
    //       })
    //     }
    //   },
    //   fail: function () {
    //     util.hideToast();
    //     that.$wuxToast.show({
    //       type: 'text',
    //       timer: 1500,
    //       color: '#fff',
    //       text: '保存失败',
    //       success: () => console.log('保存失败')
    //     })
    //   }
    // })
  }
})
