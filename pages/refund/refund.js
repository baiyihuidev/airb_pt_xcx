// pages/refund/refund.js
const util = require("../../utils/util.js");

var app = getApp();
var that;
  
Page({
  /**
   * 页面的初始数据
   */
  data: {
    reasons: ["无法安装申请退款", "更换型号重新下单","其他原因"],
    reason:"无法安装申请退款",
    value:[0],
    source:"",
    orderId:"",
    refundRemark:'',
    refundName: '',
    refundPhone: '',
  },
  onLoad:function(options){
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    // var reasons = that.data.reasons;
    // for (let i = 0; i <= reasons.length; i++) {
    //   reasons.push(i)
    // }
    that.setData({
      orderId: options.orderId,
    })
    console.log("orderId:" + that.data.orderId);
    if (app.globalData.refundRemark!=""){
      that.setData({
        refundRemark: app.globalData.refundRemark
      })
    }
  },
  //
  refundNameFunction: function (e) {
    console.log(JSON.stringify(e));
    this.setData({
      refundName: e.detail.value
    });
  },
  //备注
  refundPhoneFunction: function (e) {
    this.setData({
      refundPhone: e.detail.value
    });
  },
  //备注
  refundRemarkFunction:function(e){
    this.setData({
      refundRemark: e.detail.value
    });
    app.globalData.refundRemark = e.detail.value;
  },
  //退款按钮
  refundOrder:function(e){
    let refundName = that.data.refundName;
    let refundPhone = that.data.refundPhone;
    let refundRemark = that.data.refundRemark;
    if (refundName == "") {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请输入退货人姓名',
      })
      return;
    }
    if (refundPhone == "") {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请输入手机号码',
      })
      return;
    }

    if (!(/^1[34578]\d{9}$/.test(refundPhone))) {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请输入正确手机号码',
        success: () => console.log('请输入正确手机号码')
      })
      return;
    }

    if (refundRemark==""){
      let refundRemark = "无";
    }
    console.log("refundRemark:" + that.data.refundRemark);
    console.log("that.data.refundName:" + that.data.refundName);
    console.log("that.data.refundPhone:" + that.data.refundPhone);
    console.log("that.data.reason:" + that.data.reason);
    console.log("that.data.orderId:" + that.data.orderId);
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order/refundApplication/' + that.data.orderId,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        'recvName': that.data.refundName,
        'recvPhone': that.data.refundPhone,
        'selectReason': that.data.reason,
        'otherReason': refundRemark
      },
      success: function (res) {
        var code = res.data.code;
        if (code == 200) {
          util.hideToast();
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '提交成功',
            success: () => wx.navigateBack()
          })
        } else {
          util.hideToast();
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '提交失败',
            success: () => console.log(res.data.msg)
          })
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    })
  },
  //退款原因选择
  bindChange: function (e) {
    console.log(JSON.stringify(e));
    var val = e.detail.value;
    var reasons = that.data.reasons;
    console.log(this.data.reasons[val[0]]);
    this.setData({
      reason: this.data.reasons[val[0]],
      value: val
    })
  },
})