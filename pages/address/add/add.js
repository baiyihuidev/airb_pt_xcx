const util = require("../../../utils/util.js");

var app = getApp();
var that;

Page({
  data: {
    // 位置信息
    location: {
      latitude: "",
      longitude: ""
    },
    name: "",
    address: "",
    detailAddress: "",
    gender: 1,
    phone: "",
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    showSetting:false  
  },
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    // 属于编辑状态，
    // 上一个页面是否传递地址id，传递则为编辑地址
    // 未传递则为添加地址
    if (options.objectId) {
      that.loadAddress(options.objectId);
      that.setData({
        isEdit: true
      });
      wx.setNavigationBarTitle({
        title: '编辑地址'
      })
    } else {
      wx.setNavigationBarTitle({
        title: '新建地址'
      })
    }
  },
  onShow:function(){
    that.setData({
      showSetting: false
    })  
  },
  // 姓名
  nameInpurAction: function (e) {
    this.setData({
      name: e.detail.value
    });
  },
  // 性别
  sexRadioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value)
    this.setData({
      gender: e.detail.value
    });
  },
  // 手机号
  phoneInputAction: function (e) {
    this.setData({
      phone: e.detail.value
    });
  },
  // 详细地址
  detailAddressInpurAction: function (e) {
    this.setData({
      detailAddress: e.detail.value
    });
  },
  //获取用户地址信息时判断是否兼容canIUse
  auth:function(){
    if (that.data.canIUse) {
      that.getLocationPlace();
      console.log("that.data.canIUse");
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理  
      wx.getSetting({
        success: data => {
          if (data.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                that.getLocationPlace();
              },
              fail: res => {
                that.$wuxToast.show({
                  type: 'text',
                  timer: 2500,
                  color: '#fff',
                  text: '获取信息失败，请返回首页后重试',
                  success: () => console.log('获取信息失败，请返回首页后重试')
                })
              }
            })
          } else {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '您还未授权获取您的用户信息，请退出重试！',
              success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
            })
          }
        },
        fail: function () {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: '无法获取授权信息，请退出重试',
            success: () => console.log('无法获取授权信息，请退出重试')
          })
        }
      });
      console.log("!that.data.canIUse");
    }
  },
  //获取用户地址信息时判断是否授权
  getLocationPlace: function (res) {
    wx.authorize({
      scope: 'scope.userLocation',
      success() {
        console.log("wx.authorize:success");
        that.chooseLocation();
      },
      fail() {
        console.log("wx.authorize:fail");
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '获取定位权限失败',
          success: () => console.log('获取定位权限失败')
        })
        that.setData({
          showSetting: true
        })
      }
    })
  },
  //获取用户地址信息时已经授权，获取地址信息
  chooseLocation:function(){
    wx.chooseLocation({
      success: function (res) {
        console.log(res.address);
        var addressStr = res.address + res.name;
        that.setData({
          location: {
            latitude: res.latitude,
            longitude: res.longitude,
            name: res.name
          },
          address: addressStr
        })
      },
      fail: function (res) {
        console.log("wx.chooseLocation:fail");
        that.setData({
          location: {
            latitude: that.data.location.latitude,
            longitude: that.data.location.longitude,
            name: "重新定位",
            address: "",
            showSetting: true
          }
        })
      },
      complete: function (res) {
        console.log("complete");
      },
    })
  },
  // 添加/修改地址信息
  addAction: function (e) {
    if (that.data.name == '') {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请填写收货人姓名',
        success: () => console.log('请填写收货人姓名')
      })
      return;
    }
    if (!(/^1[34578]\d{9}$/.test(that.data.phone))) {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请填写正确手机号码',
        success: () => console.log('请填写正确手机号码')
      })
      return;
    }
    if (that.data.address == '') {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请填写收货地址',
        success: () => console.log('请填写收货地址')
      })
      return;
    }
    if (that.data.detailAddress == '') {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请填写详细地址',
        success: () => console.log('请填写详细地址')
      })
      return;
    }
    // 性别
    that.data.gender = parseInt(that.data.gender);
    //加载提示框
    util.showLoading();
    var openId = app.globalData.openId;
    wx.request({
      url: app.globalData.BaseURL + '/userAddress/addUserAddress',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        'openId': openId,
        'name': that.data.name,
        'gender': that.data.gender,
        'address': that.data.address + that.data.detailAddress,
        'phone': that.data.phone
      },
      success: function (res) {
        util.hideToast();
        var code = res.statusCode;
        if (code == 200) {
          util.hideToast();
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '保存成功',
            success: () => wx.navigateBack()
          })

        } else {
          util.hideToast();
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '保存失败',
            success: () => console.log('保存失败')
          })
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '保存失败',
          success: () => console.log('保存失败')
        })
      }
    })
  },
  loadAddress: function (objectId) {
    // 后台加载地址信息
    // that.setData({
    //   address: addressObject
    // });
  },
  delete: function () {
    // 确认删除对话框
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      cancelText: "否",
      confirmText: "是",
      confirmColor: '#00B0EB',
      success: function (res) {
        if (res.confirm) {
          var address = that.data.address;
          // 请求后台删除地址
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '删除成功',
            success: () => wx.navigateBack()
          })
        }
      }
    });
  }
})