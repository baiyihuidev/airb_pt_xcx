const util = require("../../../utils/util.js");
var app = getApp();
var that;

Page({
  data: {
    visual: 'hidden',
    addressList:""
  },
  onLoad: function (options) { 
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    if (options.isSwitchAddress) {
      that.setData({
        isSwitchAddress: true
      });
    }
  },
  onShow: function () {
    that.getAddress();
  },
  add: function () {
    wx.navigateTo({
      url: '../add/add'
    });
  },
  // 后台接口给地址赋值
  getAddress: function () {
    this.$wuxToast = app.wux(this).$wuxToast;
    //加载提示框
    util.showLoading();
    console.log("userViewId:"+app.globalData.userViewId);
    wx.request({
      url: app.globalData.BaseURL + '/userAddress/list',
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId
      }, 
      success: function (res) {
        var addressList = res.data.data;

        console.log("加载时获取的addressList:" + that.data.addressList);  
        
        util.hideToast();
        that.setData({
          addressList: addressList, 
          visual: addressList == null ? 'show' : addressList.length ? 'hidden' : 'show'
        })
        console.log("addressList:" + that.data.addressList);
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  delete: function (e) {
    var index = e.currentTarget.dataset.index;
    console.log("index:"+index);
    // var objectId = that.data.addressList[index].id;
    wx.showModal({
      title:'提示',
      content: '确认删除？',
      cancelText: "否",
      confirmText: "是",
      confirmColor:'#00B0EB',
      success:function(res){
        if (res.confirm){
          var addressList = that.data.addressList;
          var addressItemId = addressList[index].id;
          util.showLoading();
          wx.request({
            url: app.globalData.BaseURL + '/userAddress/deleteUserAddressById/'+addressItemId,
            method: 'POST',
            header: {
              'content-type': 'application/json', // 默认值
              "userViewId": app.globalData.userViewId
            },
            success: function (res) {
              util.hideToast();
              var addressList = res.data.data;
              that.setData({
                addressList: addressList,
                visual: addressList == null ? 'show':addressList.length ? 'hidden' : 'show'
              })
              console.log("删除后的addressList:" + that.data.addressList);  
            },
            fail: function () {
              util.hideToast();
              that.$wuxToast.show({
                type: 'text',
                timer: 1500,
                color: '#fff',
                text: '请求错误',
                success: () => console.log('请求错误')
              })
            }
          })
        }
      }
    })
  }, 
  selectAddress: function (e) {
    if (!that.data.isSwitchAddress) {
      return;
    }
    var index = e.currentTarget.dataset.index;
    app.globalData.addressList = that.data.addressList[index];
    
    wx.navigateBack({
      delta: 1
    })
    console.log("app.globalData.addressList:" + app.globalData.addressList.address);
  }
})