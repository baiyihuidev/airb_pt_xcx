const util = require("../../../utils/util.js");

var app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: "http://static.cakeboss.com.cn/wx-tian-headerIcon.svg",
    hasUserInfo: false,
    nickName: "获取用户信息",//用户姓名
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    codeCount: 0,
    ptCount: 0,
    integralCount: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    that.setData({
      hasUserInfo: app.globalData.hasUserInfo,
    });
    // 检查openId
    // util.checkOpenId(that);
    console.log("app.globalData.hasUserInfo======" + app.globalData.hasUserInfo);
    if (app.globalData.hasUserInfo) {
      that.setData({
        avatarUrl: app.globalData.avatarUrl,
        nickName: app.globalData.nickName
      });
    }
  },
  onShow: function (options) {
    if (app.globalData.userViewId) {
      that.setData({
        hasUserInfo: true
      });
      console.log("个人中心ordering请求时发送的userViewId:" + app.globalData.userViewId);
      that.ordering();
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
      // 所以此处加入 callback 以防止这种情况  
      app.userInfoReadyCallback = res => {
        var userInfo = JSON.parse(res.data.data);
        var userViewId = userInfo.userViewId;
        that.setData({
          hasUserInfo: true
        });
        app.globalData.userViewId = userViewId;
        console.log("2个人中心ordering请求时发送的userViewId:" + userViewId);
        that.ordering();
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理  
      wx.getSetting({
        success: data => {
          if (data.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                var userInfo = JSON.parse(res.data.data);
                var userViewId = userInfo.userViewId;
                that.setData({
                  hasUserInfo: true
                });
                app.globalData.userViewId = userViewId;
                console.log("3个人中心ordering请求时发送的userViewId:" + userViewId);
                that.ordering();
              },
              fail: res => {
                that.$wuxToast.show({
                  type: 'text',
                  timer: 2500,
                  color: '#fff',
                  text: '获取信息失败，请退出后重试',
                  success: () => console.log('获取信息失败，请退出后重试')
                })
              }
            })
          } else {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '您还未授权获取您的用户信息，请退出重试！',
              success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
            })
          }
        },
        fail: function () {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: '无法获取授权信息，请退出重试',
            success: () => console.log('无法获取授权信息，请退出重试')
          })
        }
      });
    }
  },
  // 获取个人信息
  getUserInfoAction: function (cb) {
    // 获取用户授权
    wx.getSetting({
      success: data => {
        if (data.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            withCredentials: true, 
            success: function (data) {
              console.info("2成功获取用户返回数据");
              console.info(data.userInfo);
              var userInfo = data.userInfo
              var nickName = userInfo.nickName
              var avatarUrl = userInfo.avatarUrl
              app.globalData.nickName = nickName;
              app.globalData.userInfo = userInfo;
              app.globalData.avatarUrl = avatarUrl;
              app.globalData.hasUserInfo = true;
              that.setData({
                avatarUrl: avatarUrl,
                nickName: nickName
              });
              console.log("nickName========" + that.data.nickName);
            },
            fail: function () {
              return;
            }
          });
        }
      },
      fail: function () {
        return;
      }
    });
  },
  //请求数据，列表中有待支付订单时，显示红点，提醒支付
  ordering:function(){
    wx.request({
      url: app.globalData.BaseURL + '/order/ordering',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code;
        var statusCode = res.data.data;
        console.log(statusCode);
        if (code == 200) {
          that.setData({
            codeCount: statusCode.codeCount,
            ptCount: statusCode.ptCount,
            integralCount: 0
          })
        }
      }
    })
  }
})