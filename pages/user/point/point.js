const util = require("../../../utils/util.js");

var app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userIntegral:null, //用户积分
    source:"",
    visual: 'hidden',
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/integral/getIntegralSpendList',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        util.hideToast();
        var sourceData = res.data.data;
        var code = res.data.code;
        if (code == 200) {
          that.setData({
            userIntegral: sourceData.userIntegral,
            source: sourceData.integralSpendList,
            visual: sourceData.integralSpendList == null ? 'show' : sourceData.integralSpendList.length ? 'hidden' : 'show'
          });
        } 
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    })
  },
})