const util = require("../../../../utils/util.js");
var app = getApp();
var that;

Page({
  data: {
    // 上一页商品内容
    dataSource: "",
    // 地址
    addressList: "",
    remarkStr: "",
    // 是否从地址列表返回
    refreshLish: "false",
    //区分亲友码购买、拼团购买
    tap: "",
    goodsId: ""
  },
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    // 页面初始化 options为页面跳转所带来的参数
    wx.showNavigationBarLoading();
    wx.setNavigationBarTitle({
      title: "创建订单"
    });
    // var addressList = app.globalData.addressList;
    that.setData({
      tap: app.globalData.tap,
      dataSource: JSON.parse(app.globalData.chooseGoodsDtoList),
      addressList: app.globalData.addressList
    });
    console.log("tap=====" + app.globalData.tap);
    console.log("dataSource=====" + that.data.dataSource);
    console.log("addressList=====" + app.globalData.addressList);
    console.log("userViewId:" + app.globalData.userViewId);
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/userAddress/list',
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var addressList = res.data.data;
        util.hideToast();
        console.log("addressList:" + that.data.addressList);
        if (addressList.length) {
          that.setData({
            addressList: addressList[0],
          })
          app.globalData.addressList = addressList[0];
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '地址信息请求错误',
          success: () => console.log('地址信息请求错误')
        })
      }
    })
    util.hideToast();
  },
  // 页面出现
  onShow: function () {
    that.setData({
      remarkStr: app.globalData.remarkStr,
      addressList: app.globalData.addressList
    })
    console.log("globalRemarkStr:" + app.globalData.remarkStr);
    console.log("页面出现.");
  },

  // 确认下单
  confirmOrderAction: function (e) {
    //加载提示框
    util.showLoading();
    if (that.data.addressList == "") {
      util.hideToast();
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请选择收货地址',
        success: () => console.log('收货地址')
      })
      return;
    }

    var recvAddr = app.globalData.addressList.address;
    var recvName = app.globalData.addressList.name;
    var recvPhone = app.globalData.addressList.phone;
    var goodsId = app.globalData.goodsId;
    var remarkStr = that.data.remarkStr;
    var userViewId = app.globalData.userViewId;
    console.log("创建订单页获取的globalGoodsId:" + app.globalData.goodsId);
    if (remarkStr == "" || remarkStr == undefined) {
      remarkStr = "无";
    }
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/integral/createIntegralOrder',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        'goodsId': goodsId,
        'recvAddr': recvAddr,
        'recvName': recvName,
        'recvPhone': recvPhone,
        'userViewId': app.globalData.userViewId,
        'remark': remarkStr
      },
      success: function (res) {
        util.hideToast();
        var code = res.data.code;
        var viewId = res.data.data.viewId;
        if (code == 200) {
          util.hideToast();
          var payPoint = that.data.dataSource.goods.payIntegral;
          wx.showModal({
            title: '消费提醒',
            content: '您将消费' + payPoint +'堡贝',
            confirmColor: '#00B0EB',
            cancelText:"我再想想",
            confirmText:"确认消费",
            success: function (res1) {
              util.hideToast();
              if (res1.confirm) {
                util.showLoading();
                wx.request({
                  url: app.globalData.BaseURL + '/integral/' + viewId, 
                  method: 'POST',
                  header: {
                    'content-type': 'application/json', // 默认值
                    "userViewId": app.globalData.userViewId
                  },
                  success: function (res2) {
                    util.hideToast();
                    var orderId = res2.data.data.id;
                    if (res2.data.code==200){ 
                      wx.showModal({
                        title: '兑换成功',
                        content: '兑换成功，请在个人中心，我的订单中查看订单。',
                        confirmColor: '#00B0EB',
                        cancelText: "返回首页",
                        confirmText: "查看订单",
                        success: function (res3) {
                          if (res3.confirm) {
                            wx.redirectTo({
                              url: '/pages/order/orderDetail/pointOrderDetail/pointOrderDetail?orderId=' + orderId
                            })
                          }else if (res3.cancel) {
                            console.log(1111111111111);
                            wx.switchTab({
                              url: '/pages/index/index'
                            })
                          }
                        }
                      })
                    }else{
                      util.hideToast();
                      wx.showModal({
                        title: '兑换失败',
                        content: res2.data.msg,
                        confirmColor: '#00B0EB',
                        cancelText: "我知道了",
                        confirmText: "联系客服",
                        success: function (res3) {
                          if (res3.confirm) {
                            wx.makePhoneCall({
                              phoneNumber: app.globalData.phoneNumber,
                            })
                          } else {
                            return;
                          }
                        }
                      })
                    }
                  },
                  fail: function () {
                    util.hideToast();
                    that.$wuxToast.show({
                      type: 'text',
                      timer: 1500,
                      color: '#fff',
                      text: '请求错误',
                      success: () => console.log('请求错误')
                    })
                  }
                })
              } else if (res1.cancel){
                return;
              }
            }
          })
        } else {
          util.hideToast();
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: res.data.msg,
            success: () => console.log(res.data.msg)
          })
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  // 备注
  remarkAction: function (e) {
    that.setData({
      remarkStr: e.detail.value
    });
    app.globalData.remarkStr = e.detail.value;
  },
  onReady: function () {
    // 页面渲染完成
    wx.hideNavigationBarLoading();
  },
  onReachBottom: function () {
  },
  nono: function (e) {

  },
})

