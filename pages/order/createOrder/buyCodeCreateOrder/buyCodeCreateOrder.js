const util = require("../../../../utils/util.js");
var app = getApp();
var that;

Page({
  data: {
    // 上一页商品内容
    dataSource:[],
    //请求后台商品内容
    dataDictionary:{},
    // 地址
    addressList: "",
    // 优惠券
    couponStr: "",
    remarkStr: "",
    // 是否从地址列表返回
    // refreshLish: "false",
    //区分亲友码购买、拼团购买
    tap:""
  },
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    // 页面初始化 options为页面跳转所带来的参数
    wx.showNavigationBarLoading();
    wx.setNavigationBarTitle({
      title: "创建订单"
    });
    // var addressList = app.globalData.addressList;
    that.setData({
      tap: app.globalData.tap,
      dataSource: app.globalData.chooseGoodsDtoList,
      addressList: app.globalData.addressList
    });
    console.log("tap=====" + that.data.tap);
    console.log("dataSource=====" + that.data.dataSource);
    console.log("addressList=====" + app.globalData.addressList);
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order',
      method: 'GET',
      data: {
        "goodsInfo": that.data.dataSource,
        "type": that.data.tap
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // var dataDictionary = JSON.parse(res.data.data);
        var dataDictionary = res.data.data;
        that.setData({
          dataDictionary: dataDictionary
        })
        console.log("dataDictionary:" + that.data.dataDictionary.totalPrice);
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
    console.log("userViewId:" + app.globalData.userViewId);
    wx.request({
      url: app.globalData.BaseURL + '/userAddress/list',
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var addressList = res.data.data;
        util.hideToast();
        console.log("addressList:" + that.data.addressList);
        if (addressList.length) {
          that.setData({
            addressList: addressList[0],
          })
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '地址信息请求错误',
          success: () => console.log('地址信息请求错误')
        })
      }
    })
    util.hideToast();
  },
  // 页面出现
  onShow: function () {
    that.setData({
      remarkStr: app.globalData.remarkStr
    })
    console.log("globalremarkStr:" + app.globalData.remarkStr);
    // 已选择地址
    // if (that.data.refreshLish == "true") {
      that.setData({
        addressList: app.globalData.addressList
      });
    // }
    console.log("页面出现.");
  },
  // 确认下单
  confirmOrderAction: function (e) {
    //加载提示框
    util.showLoading();
    var recvAddr = app.globalData.addressList.address;
    var recvName = app.globalData.addressList.name;
    var recvPhone = app.globalData.addressList.phone;
    var remarkStr = that.data.remarkStr;

    if (!recvAddr) {
      recvAddr = "无"
    }

    if (!recvName) {
      recvName = "无"
    }

    if (!recvPhone) {
      recvPhone = "无"
    }

    if (remarkStr == "" || remarkStr == undefined) {
      remarkStr = "无";
    }

    wx.request({
      url: app.globalData.BaseURL + '/order',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        'type': that.data.tap,
        'goodsInfo': that.data.dataSource,
        'recvAddress': recvAddr,
        'recvName': recvName,
        'recvPhone': recvPhone,
        'recvGender': that.data.addressList.gender,
        'remark': remarkStr
      },
      success: function (res) {
        var code = res.data.code;
        if (code == 200) {
          var viewId = res.data.data.viewId;
          var orderId = res.data.data.id;
          console.log("/order---viewId:" + viewId);
          wx.request({
            url: app.globalData.BaseURL + '/wx/prepayId',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: {
              "openId": app.globalData.openId,
              "orderViewId": viewId
            },
            success: function (res) {
              let prepayData = res.data.data;
              console.log("/prepayData---res:" + prepayData);
              wx.requestPayment({
                'timeStamp': prepayData.timeStamp,
                'nonceStr': prepayData.nonceStr,
                'package': prepayData.package,
                'signType': 'MD5',
                'paySign': prepayData.paySign,
                'success': function (res) {
                  app.globalData.ptInfoId = null;
                  wx.redirectTo({
                    url: '/pages/order/orderDetail/codeOrderDetail/codeOrderDetail?orderId=' + orderId,
                  })
                },
                'fail': function (res) {
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: '支付失败',
                    success: () => console.log('支付失败')
                  })
                  wx.redirectTo({
                    url: '/pages/order/orderList/orderList?type=' + 1,
                  })
                }
              })
            }
          })
        } else {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '下单失败',
            success: () => console.log('下单失败')
          })
        }
      }
    })
  },
  // 备注
  remarkAction: function (e) {
    that.setData({
      remarkStr: e.detail.value
    });
    app.globalData.remarkStr = e.detail.value;
  },
  onReady: function () {
    // 页面渲染完成
    wx.hideNavigationBarLoading();
  },
})

