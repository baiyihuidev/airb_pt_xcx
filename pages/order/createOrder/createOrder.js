const util = require("../../../utils/util.js");
var app = getApp();
var that;

Page({
  data: {
    // 上一页商品内容
    dataSource:[],
    //请求后台商品内容
    dataDictionary:{},
    // 地址
    addressList: "",
    // 优惠券
    couponStr: "",
    // 发票
    fpInfo: "",
    //纳税人识别号
    nsrInfo: "",
    remarkStr: "",
    //推荐人手机号码
    refereePhoneStr: "",
    // 是否从地址列表返回
    refreshLish: "false",
    //区分亲友码购买、拼团购买
    tap:"",
    groupId:""
  },
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    // 页面初始化 options为页面跳转所带来的参数
    wx.showNavigationBarLoading();
    wx.setNavigationBarTitle({
      title: "创建订单"
    });
    // var addressList = app.globalData.addressList;
    that.setData({
      tap: app.globalData.tap,
      groupId: app.globalData.groupId,
      ptInfoId: app.globalData.ptInfoId,
      dataSource: app.globalData.chooseGoodsDtoList,
      addressList: app.globalData.addressList
    });
    console.log("openId=====" + app.globalData.openId);
    console.log("dataSource=====" + that.data.dataSource);
    console.log("addressList=====" + app.globalData.addressList);
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order',
      method: 'GET',
      data: {
        "goodsInfo": that.data.dataSource,
        "type": that.data.tap,
        "ptId": that.data.groupId
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // var dataDictionary = JSON.parse(res.data.data);
        var dataDictionary = res.data.data;
        that.setData({
          dataDictionary: dataDictionary
        })
        console.log("dataDictionary:" + that.data.dataDictionary);
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
    console.log("userViewId:" + app.globalData.userViewId);
    wx.request({
      url: app.globalData.BaseURL + '/userAddress/list',
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var addressList = res.data.data;
        util.hideToast();
        console.log("addressList:" + that.data.addressList);
        if (addressList.length) {
          that.setData({
            addressList: addressList[0],
          })
          app.globalData.addressList = addressList[0];
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '地址信息请求错误',
          success: () => console.log('地址信息请求错误')
        })
      }
    })
    util.hideToast();
  },
  // 页面出现
  onShow: function () {
    that.setData({
      fpInfo: app.globalData.fpInfo,
      nsrInfo: app.globalData.nsrInfo,
      remarkStr: app.globalData.remarkStr
    })
    if (app.globalData.addressList != "") {
      that.setData({
        addressList: app.globalData.addressList
      })
    }

    console.log("globalfpInfo:" + app.globalData.remarkStr);
    console.log("页面出现.");
  },
  
  // 确认下单
  confirmOrderAction: function (e) {
    if (that.data.addressList == "") {
      util.hideToast();
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请选择收货地址',
        success: () => console.log('收货地址')
      })
      return;
    }
    if (that.data.addressList == "") {
      util.hideToast();
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请选择收货地址',
        success: () => console.log('收货地址')
      })
      return;
    }
    if (that.data.refereePhoneStr && !(/^1[34578]\d{9}$/.test(that.data.refereePhoneStr))) {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '推荐人手机号码不正确',
        success: () => console.log('推荐人手机号码不正确')
      })
      return;
    }
    var recvAddr = app.globalData.addressList.address;
    var recvName = app.globalData.addressList.name;
    var recvPhone = app.globalData.addressList.phone;
    var ptInfoId = that.data.ptInfoId;
    var remarkStr = that.data.remarkStr;
    var fpInfo = that.data.fpInfo;
    var nsrInfo = that.data.nsrInfo;
    var orderId = that.data.orderId;
    var refereePhoneStr = that.data.refereePhoneStr;
    if (remarkStr == "" || remarkStr == undefined) {
      remarkStr = "无";
    }
    if (fpInfo == "" || fpInfo == undefined) {
      fpInfo = "无";
    }
    if (nsrInfo == "" || nsrInfo == undefined) {
      nsrInfo = "无";
    }
    if (refereePhoneStr == "" || refereePhoneStr == undefined) {
      refereePhoneStr = "无";
    }
    // 已选择优惠券  
    var couponId = "0";
    if (that.data.couponStr != "") {
      couponId = that.data.couponStr.id;
    }
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        'type': 2,
        'ptId': that.data.groupId, 
        'goodsInfo':that.data.dataSource,
        'recvAddress': recvAddr,
        'recvName': recvName,
        'recvPhone': recvPhone,
        'recvGender': that.data.addressList.gender,
        'invoiceInformation': fpInfo,
        'identificationNumber': nsrInfo,
        'remark': remarkStr,
        'ptInfoId': ptInfoId,
        'recvMovephone': refereePhoneStr,
      },
      success: function (res) {
        var code = res.data.code;
        console.log(code,"code");
        let orderId = res.data.data.id;
        console.log("/prepayData---orderId:" + orderId);
        if (code == 200) {
          var order = res.data.data;
          console.log("/order---viewId:" + res.data.data);
          wx.request({
            url: app.globalData.BaseURL + '/wx/prepayId',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: {
              "openId": app.globalData.openId,
              "orderViewId": order.viewId
            },
            success: function (res) {
              if(res.data.code==200){
                let prepayData = res.data.data;
                console.log("/prepayData---res:" + JSON.stringify(prepayData));

                wx.requestPayment({
                  'timeStamp': prepayData.timeStamp,
                  'nonceStr': prepayData.nonceStr,
                  'package': prepayData.package,
                  'signType': 'MD5',
                  'paySign': prepayData.paySign,
                  'success': function (res) {
                    app.globalData.ptInfoId = null;
                    wx.redirectTo({
                      url: '/pages/pintuan/index?ptInfoId=' + ptInfoId,
                    })
                  },
                  'fail': function (res) {
                    that.$wuxToast.show({
                      type: 'text',
                      timer: 1500,
                      color: '#fff',
                      text: '支付失败',
                      success: () => console.log('支付失败')
                    });
                    app.globalData.ptInfoId = null;
                    wx.redirectTo({
                      url: '/pages/order/orderDetail/orderDetail?orderId=' + orderId,
                    })
                  }
                })
              }else{
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: res.data.msg,
                  success: () => console.log('res.data.msg')
                })
              }
            }
          })
        } else {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: "下单失败",
            success: () => console.log('下单失败')
          })
        }
      }
    })
  },
  // 发票
  fapiaoInput: function (e) {
    that.setData({
      fpInfo: e.detail.value
    });
    app.globalData.fpInfo = e.detail.value;
    console.log("fpInfo:" + app.globalData.fpInfo);
  },
  // 纳税人识别号
  nsrsbhInput: function (e) {
    that.setData({
      nsrInfo: e.detail.value
    });
    app.globalData.nsrInfo = e.detail.value;
  },
  // 备注
  remarkAction: function (e) {
    that.setData({
      remarkStr: e.detail.value
    });
    app.globalData.remarkStr = e.detail.value;
  },
  // 推荐人手机号码
  refereePhoneStrAction: function (e) {
    that.setData({
      refereePhoneStr: e.detail.value
    });
  },
  onReady: function () {
    // 页面渲染完成
    wx.hideNavigationBarLoading();
  }
})

