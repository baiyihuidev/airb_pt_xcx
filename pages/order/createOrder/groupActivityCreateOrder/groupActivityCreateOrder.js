const util = require("../../../../utils/util.js");
var app = getApp();
var that;

Page({
  data: {
    // 上一页商品内容
    dataSource: [], 
    //请求后台商品内容
    dataDictionary:{},
    // 地址
    // addressList: "",
    //电话
    phone:'',
    //姓名
    name:'',
    // 发票
    fpInfo: "",
    //纳税人识别号
    nsrInfo: "",
    remarkStr: "",
    //周年拼团付定活动购买（区分亲友码购买、拼团购买、周年拼团付定活动购买）
    tap:"",
    ifHasCode:'',
    isOrder: false,
    // 优惠券
    couponItem: "",
    //优惠券Id
    relativeCodeId:'',
    //创建订单时是否选择了亲友码
    loadCouponListPage:"",
    oldGoodsId: "",//分享链接给老用户时，老用户点击进入的goodsId
    totalNum:''//从上页获取的支付的定金数量
  },
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    var tap = options.tap;
    var num = options.num;
    console.log(options.tap,"tap");
    // 页面初始化 options为页面跳转所带来的参数
    wx.showNavigationBarLoading();
    wx.setNavigationBarTitle({
      title: "创建订单"
    });
    console.log("页面加载");
    console.log(num,'num');
    that.setData({
      dataSource: app.globalData.chooseGoodsDtoList
    });
    if (tap) {
      that.setData({
        tap: tap,
      });
    }
    if(num){
      that.setData({
        totalNum: num,
      });
    }
    console.log("openId=====" + app.globalData.openId);
    console.log("dataSource=====" + that.data.dataSource);
    console.log("tap=====" + that.data.tap);

    if (app.globalData.couponItem) {//从个人中心-全部亲友码页点击“去使用”跳转进来的
      that.setData({
        couponItem: app.globalData.couponItem
      })
    }
    var requestOrderData = {
      "goodsInfo": that.data.dataSource,
      "type": that.data.tap
    }
    console.log("requestOrderData:"+JSON.stringify(requestOrderData));
    wx.request({
      url: app.globalData.BaseURL + '/order',
      method: 'GET',
      data: requestOrderData,
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId,
      },
      success: function (res) {
        // var dataDictionary = JSON.parse(res.data.data);
        var dataDictionary = res.data.data;
        that.setData({
          dataDictionary: dataDictionary,
          ifHasCode: dataDictionary.ifHasCode,
          relativeCodeId: dataDictionary.codeInfoShow.relativeCodeId,
        })
        console.log("dataDictionary:" + that.data.dataDictionary.totalPrice);
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
    console.log("userViewId:" + app.globalData.userViewId);
    // wx.request({
    //   url: app.globalData.BaseURL + '/userAddress/list',
    //   method: 'GET',
    //   header: {
    //     'content-type': 'application/json', // 默认值
    //     "userViewId": app.globalData.userViewId
    //   },
    //   success: function (res) {
    //     var addressList = res.data.data;
    //     util.hideToast();
    //     console.log("addressList:" + that.data.addressList);
    //     if (addressList.length){
    //       that.setData({
    //         addressList: addressList[0],
    //       })
    //       app.globalData.addressList = addressList[0];
    //     }
    //   },
    //   fail: function () {
    //     util.hideToast();
    //     that.$wuxToast.show({
    //       type: 'text',
    //       timer: 1500,
    //       color: '#fff',
    //       text: '地址信息请求错误',
    //       success: () => console.log('地址信息请求错误')
    //     })
    //   }
    // })
    util.hideToast();
  },
  // 页面出现
  onShow: function () {
    that.setData({
      remarkStr: app.globalData.remarkStr
    })
    if (app.globalData.loadCouponListPage){//从亲友码列表页返回
      that.setData({
        couponItem: app.globalData.couponItem,
        loadCouponListPage: app.globalData.loadCouponListPage
      })
    }
    // if (app.globalData.addressList!=""){
    //   that.setData({
    //     addressList: app.globalData.addressList
    //   })
    // }
    console.log("onshow-app.globalData.fpInfo:" + app.globalData.fpInfo);
    if (app.globalData.fpInfo!=""){
      that.setData({
        fpInfo: app.globalData.fpInfo,
        nsrInfo: app.globalData.nsrInfo
      })
    }  
    console.log("onShow:app.globalData.relativeCodeId:" + app.globalData.relativeCodeId);
    console.log("onShow:app.globalData.couponItem:" + app.globalData.couponItem);
    if (app.globalData.loadCouponListPage){//从优惠券列表页选择了优惠券跳转进来
      console.log("couponItem=========="+app.globalData.couponItem);
      that.setData({
        couponItem: app.globalData.couponItem,
        relativeCodeId: app.globalData.relativeCodeId
      })
      util.showLoading();
      wx.request({
        url: app.globalData.BaseURL + '/order',
        method: 'GET',
        data: {
          "goodsInfo": that.data.dataSource,
          "type": that.data.tap,
          "relativeCodeInfoId": app.globalData.relativeCodeId,
        },
        header: {
          'content-type': 'application/json', // 默认值
          "userViewId": app.globalData.userViewId
        },
        success: function (res) {
          var dataDictionary = res.data.data;
          util.hideToast();
          that.setData({
            dataDictionary: dataDictionary,
            ifHasCode: dataDictionary.ifHasCode,
            relativeCodeId: dataDictionary.codeInfoShow.relativeCodeId,
          })
          if (!dataDictionary.available && app.globalData.relativeCodeId != -1){
            that.setData({
              relativeCodeId: 0,
            })
            wx.showModal({
              title: '提示',
              content: '您选择的亲友码不满足满减条件',
              confirmColor: '#00B0EB',
              cancelText: "继续下单",
              confirmText: "更换券码",
              success: function (res) {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/coupon/list/list',
                  })
                }
              }
            });
          }
          console.log("从优惠券列表页选择了优惠券跳转进来dataDictionary:" + that.data.dataDictionary.codeInfoShow.relativeCodeId);
        },
        fail: function () {
          util.hideToast();
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '请求错误',
            success: () => console.log('请求错误')
          })
        }
      })
    } 
    console.log("globalfpInfo:" + app.globalData.remarkStr);
    console.log("页面出现.");
  },
  
  // 确认下单
  confirmOrderAction: function (e) {
    if (that.data.name == "") {
      util.hideToast();
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请填写姓名',
        success: () => console.log('请填写姓名')
      })
      return;
    }
    if (that.data.phone == "") {
      util.hideToast();
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请填写电话',
        success: () => console.log('请填写电话')
      })
      return;
    }
    if (!(/^1[34578]\d{9}$/.test(that.data.phone))) {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '手机号码不正确',
        success: () => console.log('手机号码不正确')
      })
      return;
    }
    // var recvAddr = app.globalData.addressList.address;
    var recvName = that.data.name;
    var recvPhone = that.data.phone;
    var remarkStr = that.data.remarkStr;
    var fpInfo = that.data.fpInfo;
    var nsrInfo = that.data.nsrInfo;
    if (remarkStr == "" || remarkStr == undefined) {
      remarkStr = "无";
    }
    if (fpInfo == "" || fpInfo == undefined) {
      fpInfo = "无";
    }
    if (nsrInfo == "" || nsrInfo == undefined) {
      nsrInfo = "无";
    }
    // 已选择优惠券  
    // var couponId = "0";
    // if (that.data.couponStr != "") {
    //   couponId = that.data.couponStr.id;
    // }
    //加载提示框
    util.showLoading();
    //判断定金是否符合支付尾款资格
    wx.request({
      url: app.globalData.BaseURL + '/activity/xcx/judgeActivityAddOrder',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: { 'num': that.data.totalNum },
      success: function (res) {
        if (res.data.code == 200) {
          console.log("/order--data--tap:" + that.data.tap);
          console.log("/order---relativeCodeId:" + that.data.relativeCodeId);
          //生成订单
          wx.request({
            url: app.globalData.BaseURL + '/order',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: {
              'type': that.data.tap,
              'goodsInfo': that.data.dataSource,
              'recvAddress': '无',
              'recvName': recvName,
              'recvPhone': recvPhone,
              'recvGender': 1,
              'invoiceInformation': fpInfo,
              'identificationNumber': nsrInfo,
              'remark': remarkStr,
              'relativeCodeInfoId': that.data.relativeCodeId,
            },
            success: function (res) {
              var code = res.data.code;
              if (code == 200) {
                var viewId = res.data.data.viewId;
                var orderId = res.data.data.id;
                console.log("/order---viewId:" + viewId);
                console.log("/order---app.globalData.openId:" + app.globalData.openId);
                console.log("/order---orderId:" + orderId);
                wx.request({
                  url: app.globalData.BaseURL + '/wx/prepayId',
                  method: 'POST',
                  header: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "userViewId": app.globalData.userViewId
                  },
                  data: {
                    "openId": app.globalData.openId,
                    "orderViewId": viewId
                  },
                  success: function (res1) {
                    util.hideToast();
                    let prepayData = res1.data.data;
                    console.log("/prepayData---res:" + prepayData);
                    wx.requestPayment({
                      'timeStamp': prepayData.timeStamp,
                      'nonceStr': prepayData.nonceStr,
                      'package': prepayData.package,
                      'signType': 'MD5',
                      'paySign': prepayData.paySign,
                      'success': function (res2) {
                        app.globalData.couponItem = null;
                        app.globalData.relativeCodeId = null;
                        app.globalData.loadCouponListPage = false;
                        that.$wuxToast.show({
                          type: 'text',
                          timer: 1500,
                          color: '#fff',
                          text: '支付成功',
                          success: () => console.log('支付成功')
                        })
                        wx.redirectTo({
                          url: '/pages/order/orderDetail/codeToBuyOrderDetail/codeToBuyOrderDetail?orderId=' + orderId + '&orderViewId=' + that.data.viewId + '&tap=' + that.data.tap,
                        })
                      },
                      'fail': function (res2) {
                        app.globalData.couponItem = null;
                        app.globalData.relativeCodeId = null;
                        app.globalData.loadCouponListPage = false;
                        that.$wuxToast.show({
                          type: 'text',
                          timer: 1500,
                          color: '#fff',
                          text: '支付失败',
                          success: () => console.log('支付失败')
                        });
                        wx.redirectTo({
                          url: '/pages/order/orderDetail/codeToBuyOrderDetail/codeToBuyOrderDetail?orderId=' + orderId + '&orderviewid=' + viewId,
                        })
                      },
                    })
                  }
                })
              } else {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '下单失败',
                  success: () => console.log('下单失败')
                })
              }
            }
          })
        } else {//购买资格不符
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: res.data.msg,
            success: () => console.log(res.data.msg)
          })
        }
      },
      fail: function (res) {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误，请重试~',
          success: () => console.log('请求错误，请重试~')
        })
      }
    })
  },
  // 姓名
  nameAction: function (e) {
    that.setData({
      name: e.detail.value
    });
  },
  // 电话
  phoneAction: function (e) {
    that.setData({
      phone: e.detail.value
    });
  },
  // 备注
  remarkAction: function (e) {
    that.setData({
      remarkStr: e.detail.value
    });
    app.globalData.remarkStr = e.detail.value;
  },
  onReady: function () {
    // 页面渲染完成
    wx.hideNavigationBarLoading();
  },
})

