// pages/user/index/user.js

const util = require("../../../../utils/util.js");

var app = getApp();
var that;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    orderId: "",
    sourceData:"",
    tap:"",
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
    that.setData({
      orderId: options.orderId,
    });
    if (options.tap){
      that.setData({
        tap: options.tap
      });
    }
    console.log("详情页读取的orderId:"+that.data.orderId);
    that.getDataSource();
  },
  // 请求页面数据
  getDataSource: function () {
    var orderId = that.data.orderId;
    //加载提示框
    util.showLoading();
    var urlStr = '/order/getOrderRelativeDetail/' + orderId;
    // var urlStr = '/order/getOrderRelativeDetail/' + 374;
    var tap;
    if (app.globalData.tap){
      tap = app.globalData.tap;
    } else if (that.data.tap){
      tap = that.data.tap;
    }
    console.log(tap,"=============")
    wx.request({
      url: app.globalData.BaseURL + urlStr + '?type=' + tap,
      method: 'GET',
      success:function(res){
        util.hideToast();
        var code = res.statusCode;
        if(code==200){
          util.hideToast();
          that.setData({
            sourceData: res.data.data
          });
          console.log("亲友码详情页获取的数据:" + that.data.sourceData);
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    })
  },
  //分享
  onShareAppMessage: function (options) {
    console.log("options.from：" + options.from);
    if (options.from === 'button') {
      var couponId = options.target.id;
      var couponPrice = options.target.dataset.price;
      console.log("couponId:" + couponId);
      return {
        title: "福利快戳！亲友码买空气堡能减￥" + couponPrice.split("-")[1] +"!",
        path: '/pages/detail/friendCodeToBuy/detail?couponId=' + couponId + '&tap=1',
        imageUrl: 'http://static.cakeboss.com.cn/kqbQinYouCodePostToNewUserN1.jpg',
        success: function (options) {
          util.shareSuccessTost();
        },
        fail: function (options) {
          util.shareCancelTost();
        }
      }
    } else {
      return {
        path: '/pages/index/index',
        imageUrl: 'http://static.cakeboss.com.cn/kqbQinYouCodePostToNewUserN1.jpg',
        success: function (options) {
          // 转发成功
        },
        fail: function (options) {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '取消分享'
          })
        }
      }
    }
  },
  //分享的亲友码进详情
  linkToCouponDetail: function (options) {
    var relativeCodeInfoId = options.currentTarget.id;
    var status = options.currentTarget.dataset.status;
    if (status == 1) {
      return;
    } else {
      console.log("linkToCouponDetail-couponId:" + relativeCodeInfoId);
      console.log("linkToCouponDetail-status:" + status);
      wx.navigateTo({
        url: '/pages/coupon/detail/detail?relativeCodeInfoId=' + relativeCodeInfoId + '&status=' + status,
      })
    }
  },
})