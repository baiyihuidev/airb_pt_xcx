// pages/user/index/user.js

const util = require("../../../../utils/util.js");

var app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderViewId: "",
    orderId: '',
    orderInfo: "",
    userInfo: "",
    goodsInfo: "",
    tap:'',//从活动下单页跳转过来时，tap为5
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
    that.setData({
      orderId: options.orderId,
    });
    if(options.tap){
      that.setData({
        tap: options.tap,
      });
    }
    console.log(that.data.tap,"that.data.tap");
    that.getDataSource();
  },
  // 请求页面数据
  getDataSource: function () {
    console.log("请求数据时的orderId======" + that.data.orderId);
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order/orderDetail/' + that.data.orderId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code;
        util.hideToast();
        var dataArray = res.data;
        util.hideToast();
        that.setData({
          sourceData: res.data,
        });
        console.log("拼团订单详情页状态码:" + that.data.sourceData.orders.status);
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    })
  },
  //确认收货
  confirmOrder: function (event) {
    var orderId = event.currentTarget.id;
    var task = event.currentTarget.dataset.task;
    if (task==="安装完成"){
      wx.showModal({
        title: '收货提醒',
        content: '确认收货？',
        cancelText: "否",
        confirmText: "是",
        confirmColor: '#00B0EB',
        success: function (res) {
          if (res.confirm) {
            util.showLoading();
            wx.request({
              url: app.globalData.BaseURL + '/order/confirmOrder/' + orderId,
              method: 'POST',
              header: {
                "Content-Type": "application/x-www-form-urlencoded",
                "userViewId": app.globalData.userViewId
              },
              success: function (res) {
                var code = res.data.code;
                if (code == 200) {
                  util.hideToast();
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: '操作成功',
                    success: function () {
                      that.getDataSource();
                    }
                  })
                } else {
                  util.hideToast();
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: res.data.msg,
                    success: () => console.log(res.data.msg)
                  })
                }
              },
              fail: function () {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '提交失败',
                  success: () => console.log('提交失败')
                })
              }
            })
          }
        }
      });
    }else{
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '当安装进度为“安装完成”，方可确认收货',
      })
    }
  },
  //取消订单
  cancelOrder: function (event) {
    var orderId = event.currentTarget.id;
    wx.showModal({
      title: '取消提醒',
      content: '确认取消？',
      cancelText: "否",
      confirmText: "是",
      confirmColor: '#00B0EB',
      success: function (res) {
        if (res.confirm) {
          util.showLoading();
          wx.request({
            url: app.globalData.BaseURL + '/order/cancelOrder/' + orderId,
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            success: function (res) {
              var code = res.data.code;
              if (code == 200) {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '操作成功',
                  success: function () {
                    that.getDataSource();
                  }
                })
              } else {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: res.data.msg,
                  success: () => console.log(res.data.msg)
                })
              }
            },
            fail: function () {
              util.hideToast();
              that.$wuxToast.show({
                type: 'text',
                timer: 1500,
                color: '#fff',
                text: '提交失败',
                success: () => console.log('提交失败')
              })
            }
          })
        }
      }
    });
  },
  // 继续付款
  payOrder: function (e) {
    var orderViewId = e.currentTarget.dataset.orderviewid;
    console.log("继续支付orderViewId:" + orderViewId);
    wx.request({
      url: app.globalData.BaseURL + '/wx/prepayId',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        "openId": app.globalData.openId,
        "orderViewId": orderViewId
      },
      success: function (res) {
        let prepayData = res.data.data;
        console.log("/prepayData---res:" + prepayData);
        wx.requestPayment({
          'timeStamp': prepayData.timeStamp,
          'nonceStr': prepayData.nonceStr,
          'package': prepayData.package,
          'signType': 'MD5',
          'paySign': prepayData.paySign,
          'success': function (res) {
            that.getDataSource();
          },
          'fail': function (res) {
            that.$wuxToast.show({
              type: 'text',
              timer: 1500,
              color: '#fff',
              text: '支付失败',
              success: () => console.log('支付失败')
            })
          }
        })
      }
    })
  },
})