// pages/user/index/user.js

const util = require("../../../../utils/util.js");

var app = getApp();
var that;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    orderId: "",
    sourceData:""
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
    that.setData({
      orderId: options.orderId
    });
    console.log("options.orderId+++++++++" + options.orderId);
    that.getDataSource();
  },
  // 请求页面数据
  getDataSource: function () {
    var orderId = that.data.orderId;
    console.log(orderId);
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order/orderDetail/'+orderId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code; 
        util.hideToast();
        var dataArray = res.data;
        util.hideToast();
        that.setData({
          sourceData: res.data
        });
        console.log("积分兑换商品详情页状态码:" + that.data.sourceData.orders.status);
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    })
  },
  //确认收货
  confirmOrderAction:function(){
    var orderId = that.data.orderId;
    wx.showModal({
      title: '收货提醒',
      content: '确认收货？',
      cancelText: "否",
      confirmText: "是",
      confirmColor: '#00B0EB',
      success: function (res3) {
        if (res3.confirm) {
          //加载提示框
          util.showLoading();
          var urlStr = '/order/confirmOrder/' + orderId;
          var parameters = "";
          util.request(urlStr, parameters, function (res) {
            var code = res.statusCode;
            if (code == 200) {
              that.getDataSource();
            } else {
              that.$wuxToast.show({
                type: 'text',
                timer: 1500,
                color: '#fff',
                text: '请求错误',
                success: () => console.log('请求错误')
              })
            }
          }, "POST")
        } else if (res3.cancel) {
          return;
        }
      }
    })
  }
})