// pages/user/index/user.js

const util = require("../../../utils/util.js");

var app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId:'',
    orderInfo: "",
    userInfo: "",
    goodsInfo: "",
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
    that.setData({
      orderId: options.orderId,
    });
    that.getDataSource();
  },
  // 请求页面数据
  getDataSource: function () {
    console.log("请求数据时的orderId======" + that.data.orderId);
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/order/orderDetail/' + that.data.orderId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code;
        console.log("订单详情：", res.data);
        util.hideToast();
        var dataArray = res.data;
        util.hideToast();
        // res.data.ptStatus = 3;
        // res.data.status = 13;
        that.setData({
          sourceData: res.data
        });
        console.log("拼团订单详情页状态码:" + that.data.sourceData.orders.status);
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    })
  },
  //确认收货
  confirmOrder: function (event) {
    var orderId = event.currentTarget.id;
    var task = event.currentTarget.dataset.task;
    if (task === "安装完成") {
      wx.showModal({
        title: '收货提醒',
        content: '确认收货？',
        cancelText: "否",
        confirmText: "是",
        confirmColor: '#00B0EB',
        success: function (res) {
          if (res.confirm) {
            util.showLoading();
            wx.request({
              url: app.globalData.BaseURL + '/order/confirmOrder/' + orderId,
              method: 'POST',
              header: {
                "Content-Type": "application/x-www-form-urlencoded",
                "userViewId": app.globalData.userViewId
              },
              success: function (res) {
                var code = res.data.code;
                if (code == 200) {
                  util.hideToast();
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: '操作成功',
                    success: function () {
                      that.getDataSource();
                    }
                  })
                } else {
                  util.hideToast();
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: res.data.msg,
                    success: () => console.log(res.data.msg)
                  })
                }
              },
              fail: function () {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '提交失败',
                  success: () => console.log('提交失败')
                })
              }
            })
          }
        }
      });
    } else {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '当安装进度为“安装完成”，方可确认收货',
      })
    }
  },
  //取消订单
  cancelOrder: function (event) {
    var orderId = event.currentTarget.id;
    wx.showModal({
      title: '取消订单',
      content: '确认要取消此订单吗？',
      cancelText: "否",
      confirmText: "是",
      confirmColor: '#00B0EB',
      success: function (res) {
        if (res.confirm) {
          util.showLoading();
          wx.request({
            url: app.globalData.BaseURL + '/order/cancelOrder/' + orderId,
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            success: function (res) {
              var code = res.data.code;
              if (code == 200) {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '操作成功',
                  success: function () {
                    that.getDataSource();
                  }
                })
              } else {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: res.data.msg,
                  success: () => console.log(res.data.msg)
                })
              }
            },
            fail: function () {
              util.hideToast();
              that.$wuxToast.show({
                type: 'text',
                timer: 1500,
                color: '#fff',
                text: '提交失败',
                success: () => console.log('提交失败')
              })
            }
          })
        }
      }
    });
  },
  // 继续付款
  payOrder: function (e) {
    var orderViewId = e.currentTarget.dataset.orderviewid;
    console.log("继续支付orderViewId:" + orderViewId);
    wx.request({
      url: app.globalData.BaseURL + '/wx/prepayId',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        "openId": app.globalData.openId,
        "orderViewId": orderViewId
      },
      success: function (res) {
        let prepayData = res.data.data;
        console.log("/prepayData---res:" + prepayData);
        wx.requestPayment({
          'timeStamp': prepayData.timeStamp,
          'nonceStr': prepayData.nonceStr,
          'package': prepayData.package,
          'signType': 'MD5',
          'paySign': prepayData.paySign,
          'success': function (res) {
            that.getDataSource();
          },
          'fail': function (res) {
            that.$wuxToast.show({
              type: 'text',
              timer: 1500,
              color: '#fff',
              text: '支付失败',
              success: () => console.log('支付失败')
            })
          }
        })
      }
    })
  },
  //邀请拼团
  onShareAppMessage: function (event) {
    console.log("options.from：" + event.from);
    if (event.from === 'button') {
      var ptInfoId = event.target.ptinfoid;
      return {
        title: "您的好友邀您参与限时拼团，一起享受极净好空气！",
        path: '/pages/pintuan/index?ptInfoId=' + ptInfoId,
        imageUrl:'http://static.cakeboss.com.cn/kqbPTN1.jpg',
        success: function (options) {
          util.shareSuccessTost();
        },
        fail: function (options) {
          util.shareCancelTost();
        }
      }
    } else {
      return {
        title: "空气堡",
        path: '/pages/index/index',
        imageUrl: 'http://static.cakeboss.com.cn/kqbBanner1.svg',
        success: function (options) {
          // 转发成功
        },
        fail: function (options) {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '取消分享'
          })
        }
      }
    }
  },
})