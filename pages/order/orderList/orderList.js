// pages/order/orderList.js

const util = require("../../../utils/util.js");

var app = getApp();
var that;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    visual: 'hidden',
    ptOrderList: [],
    jfOrderList: [],
    qyOrderList: [],
    // 切换tab标记
    currentTabsIndex: 0,
    //区分请求后台数据的type
    urlType:1,
    //亲友码、拼团、积分待支付、拼团中状态订单数
    statusCount:"",
    tabArr: [{ "name": "亲友计划", "count": 0 }, { "name": "拼团订单", "count": 0 }, { "name": "积分兑换", "count": 0 }]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    console.log(options,"options");
    if (options.type != undefined) {//从个人中心积分兑换点击进来
      that.setData({
        urlType: options.type
      })
      if (options.type>3){//从品牌月推送的链接进入时，type=5
        that.setData({
          currentTabsIndex: 0,
        })
      } else {//从个人中心进入
        that.setData({
          currentTabsIndex: options.type - 1,
        })
      }
    }
    console.log("that.data.urlType:"+that.data.urlType);
    if (app.globalData.userViewId) {
      that.setData({
        hasUserInfo: true
      });
      console.log("请求订单列表页数据时发送的userViewId:" + app.globalData.userViewId);
      that.getDataSource();
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
      // 所以此处加入 callback 以防止这种情况  
      app.userInfoReadyCallback = res => {
        var userInfo = JSON.parse(res.data.data);
        var userViewId = userInfo.userViewId;
        that.setData({
          hasUserInfo: true
        });
        app.globalData.userViewId = userViewId;
        console.log("2请求订单列表页数据时发送的userViewId:" + userViewId);
        that.getDataSource();
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理  
      wx.getSetting({
        success: data => {
          if (data.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                var userInfo = JSON.parse(res.data.data);
                var userViewId = userInfo.userViewId;
                that.setData({
                  hasUserInfo: true
                });
                app.globalData.userViewId = userViewId;
                console.log("3请求订单列表页数据时发送的userViewId:" + userViewId);
                that.getDataSource();
              },
              fail: res => {
                that.$wuxToast.show({
                  type: 'text',
                  timer: 2500,
                  color: '#fff',
                  text: '获取信息失败，请返回首页后重试',
                  success: () => console.log('获取信息失败，请返回首页后重试')
                })
              }
            })
          } else {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '您还未授权获取您的用户信息，请退出重试！',
              success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
            })
          }
        },
        fail: function () {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: '无法获取授权信息，请退出重试',
            success: () => console.log('无法获取授权信息，请退出重试')
          })
        }
      });
    }
  },
  onShow: function () {
    that.getDataSource();
  },
  // 请求页面数据
  getDataSource: function () {
    //加载提示框
    util.showLoading();
    console.log("userViewId:" + app.globalData.userViewId);
    console.log("加载时的currentTabsIndex:" + that.data.currentTabsIndex);
    console.log("加载时获取的urlType:" + that.data.urlType);
    wx.request({
      url: app.globalData.BaseURL + '/order/list?type=' + that.data.urlType,
      method: 'GET', 
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var orderList = res.data.data;
        var index = that.data.currentTabsIndex;
        console.log("加载时获取的订单列表:" + orderList);
        that.setData({
          visual: orderList == null ? 'show' : orderList.length ? 'hidden' : 'show'
        })
        if (index == 0) {
          that.setData({
            qyOrderList: orderList,
          })
        } else if (index == 1) {
          that.setData({
            ptOrderList: orderList,
          })
        } else {
          that.setData({
            jfOrderList: orderList,
          })
        }
        util.hideToast();
      },
      fail: function () {
        util.hideToast();
        if (index == 0) {
          that.setData({
            qyOrderList: "",
          })
        } else if (index == 1) {
          that.setData({
            ptOrderList: "",
          })
        } else {
          that.setData({
            jfOrderList: "",
          })
        }
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })

    //请求数据，列表中有待支付订单时，显示红点，提醒支付
    wx.request({
      url: app.globalData.BaseURL + '/order/ordering',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code;
        var statusCode = res.data.data;
        console.log(statusCode);
        var newTab = that.data.tabArr;
        if (code == 200) {
          newTab[0].count = statusCode.codeCount;
          newTab[1].count = statusCode.ptCount;
          newTab[2].count = 0;
          that.setData({
            tabArr: newTab
          })
        }
      }
    })
  },
  //确认收货
  confirmOrder: function (event) {
    var orderId = event.currentTarget.id;
    var task = event.currentTarget.dataset.task;
    if (task=="32"){
      wx.showModal({
        title: '收货提醒',
        content: '确认收货？',
        cancelText: "是",
        confirmText: "否",
        confirmColor: '#00B0EB',
        success: function (res) {
          if (res.confirm) {
            util.showLoading();
            wx.request({
              url: app.globalData.BaseURL + '/order/confirmOrder/' + orderId,
              method: 'POST',
              header: {
                "Content-Type": "application/x-www-form-urlencoded",
                "userViewId": app.globalData.userViewId
              },
              success: function (res) {
                var code = res.data.code;
                if (code == 200) {
                  util.hideToast();
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: '操作成功',
                    success: function () {
                      that.getDataSource();
                    }
                  })
                } else {
                  util.hideToast();
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: res.data.msg,
                    success: () => console.log(res.data.msg)
                  })
                }
              },
              fail: function () {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '提交失败',
                  success: () => console.log('提交失败')
                })
              }
            })
          }
        }
      });
    }else{
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '当安装进度为“安装完成”，方可确认收货',
      })
    }
  },
  //拼团列表点击进详情
  ptLinkToDetail: function (event) {
    var ptinfoid = event.currentTarget.dataset.ptinfoid;
    console.log("ptInfoId:" + ptinfoid);
    wx.navigateTo({
      url: '/pages/pintuan/index?ptInfoId='+ptinfoid
    })
  },
  //亲友码购机、拼团成功订单退款
  refundOrder: function (event) {
    var orderid = event.currentTarget.dataset.id;
    console.log("orderid:" + orderid);
    wx.navigateTo({
      url: '/pages/refund/refund?orderId=' + orderid
    })
  },
  //亲友码列表点击进详情
  qyLinkToDetail: function (event) {
    
    var status = event.currentTarget.dataset.status; //订单状态
    var orderId = event.currentTarget.dataset.orderid;//订单id
    var orderviewid = event.currentTarget.dataset.orderviewid;//订单viewid
    var orderType = event.currentTarget.dataset.ordertype;//亲友码商品/使用亲友码购机
    console.log("status:" + status + ",orderId:" + orderId + ",orderType:" + orderType);
    if (orderType==1){//购买亲友码
      if (status==1){//未支付
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '您还未支付',
          success: () => console.log('您还未支付')
        })
        return;
      }else{
        wx.navigateTo({
          url: '/pages/order/orderDetail/codeOrderDetail/codeOrderDetail?orderId=' + orderId + '&orderviewid=' + orderviewid + '&tap=' + orderType
        })
      }
    }else{//使用亲友码购机
      wx.navigateTo({
        url: '/pages/order/orderDetail/codeToBuyOrderDetail/codeToBuyOrderDetail?orderId=' + orderId + '&orderviewid=' + orderviewid + '&tap=' + orderType
      })
    }
  },
  //取消订单
  cancelOrder: function (event) {
    var orderId = event.currentTarget.id;
    wx.showModal({
      title: '取消订单',
      content: '确认要取消此订单吗？',
      cancelText: "否",
      confirmText: "是",
      confirmColor: '#00B0EB',
      success: function (res) {
        if (res.confirm) {
          util.showLoading();
          wx.request({
            url: app.globalData.BaseURL + '/order/cancelOrder/' + orderId,
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            success: function (res) {
              var code = res.data.code;
              if (code == 200) {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: '操作成功',
                  success: function () {
                    that.getDataSource();
                  }
                })
              } else {
                util.hideToast();
                that.$wuxToast.show({
                  type: 'text',
                  timer: 1500,
                  color: '#fff',
                  text: res.data.msg,
                  success: () => console.log(res.data.msg)
                })
              }
            },
            fail: function () {
              util.hideToast();
              that.$wuxToast.show({
                type: 'text',
                timer: 1500,
                color: '#fff',
                text: '提交失败',
                success: () => console.log('提交失败')
              })
            }
          })
        }
      }
    });
  },
  // 继续付款
  payOrder: function (e) {
    var orderViewId = e.currentTarget.dataset.orderviewid;
    console.log("继续支付orderViewId：" + orderViewId);
    wx.request({
      url: app.globalData.BaseURL + '/wx/prepayId',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: {
        "openId": app.globalData.openId,
        "orderViewId": orderViewId
      },
      success: function (res) {
        let prepayData = res.data.data;
        console.log("/prepayData---res:" + prepayData);
        wx.requestPayment({
          'timeStamp': prepayData.timeStamp,
          'nonceStr': prepayData.nonceStr,
          'package': prepayData.package,
          'signType': 'MD5',
          'paySign': prepayData.paySign,
          'success': function (res) {
            that.getDataSource();
          },
          'fail': function (res) {
            that.$wuxToast.show({
              type: 'text',
              timer: 1500,
              color: '#fff',
              text: '支付失败',
              success: () => console.log('支付失败')
            })
          }
        })
      }
    })
  },
  //切换亲友码、拼团、积分商城
  onTabsItemTap: function (e) {
    var index = e.currentTarget.dataset.index;
    console.log(index);
    that.setData({
      currentTabsIndex: index
    });
    //不同的tab，携带不同的参数请求后台数据
    if (index==0){
      that.setData({
        urlType: 1
      });
    }else if(index==1){
      that.setData({
        urlType: 2
      });
    }else{
      that.setData({
        urlType: 3
      });
    }
    that.getDataSource();
  },
  //邀请拼团
  onShareAppMessage: function (options) {
    console.log("options.from：" + options.from);
    if (options.from === 'button') {
      console.log("options.target.dataset.ptinfoid：" + options.target.dataset.ptinfoid);
      var ptInfoId = options.target.dataset.ptinfoid;
      return {
        title: "您的好友邀您参与限时拼团，一起享受极净好空气！",
        path: '/pages/pintuan/index?ptInfoId=' + ptInfoId,
        imageUrl: 'http://static.cakeboss.com.cn/kqbPTN1.jpg',
        success: function (options) {
          util.shareSuccessTost();
        },
        fail: function (options) {
          util.shareCancelTost();
        }
      }
    } else {
      return {
        title: "空气堡",
        path: '/pages/index/index',
        imageUrl: 'http://static.cakeboss.com.cn/kqbBanner1.svg',
        success: function (options) {
          // 转发成功
        },
        fail: function (options) {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '取消分享'
          })
        }
      }
    }
  },
})