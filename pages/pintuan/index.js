const util = require("../../utils/util.js");
var app = getApp();
var that;

// pages/pintuan/index.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    expireTime: "",
    clock: '',
    ptInfoId:0, 
    ptData:"",
    //判断当前订单状态---拼团中，拼团成功，拼团失败
    ptStatus:"",
    //判断当前用户是否可以购买
    ptFlag:"",
    ptId:"",
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    fromViewUserId:null,//分享人的userViewId 
    ptSize:2, //几人团 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数  
    this.$wuxToast = app.wux(this).$wuxToast;
    that = this;
    var tap = options.tap;
    var fromViewUserId = options.fromViewUserId;
    if (fromViewUserId != undefined) {
      that.setData({
        fromViewUserId: fromViewUserId
      })
    }
    console.log("fromViewUserId===" + that.data.fromViewUserId);
    // var ptData = that.data.ptData; 
    var ptInfoId = (options.ptInfoId == null || options.ptInfoId == 0) ? that.data.ptInfoId : options.ptInfoId;
    that.setData({
      ptInfoId: ptInfoId
    })
    //var ptInfoId = that.data.ptInfoId;
    console.log("ptInfoId============"+ptInfoId);
    wx.setNavigationBarTitle({
      title: "拼团"
    });
    util.showLoading();
    if (app.globalData.userViewId) {
      console.log("1请求时发送的userViewId:" + app.globalData.userViewId);
      that.getSourceData();
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
      // 所以此处加入 callback 以防止这种情况  
      app.userInfoReadyCallback = res => {
        var userInfo = JSON.parse(res.data.data);
        var userViewId = userInfo.userViewId;
        app.globalData.userViewId = userViewId;
        console.log("2请求时发送的userViewId:" + userViewId);
        that.getSourceData();
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理  
      wx.getSetting({
        success: data => {
          if (data.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                var userInfo = JSON.parse(res.data.data);
                var userViewId = userInfo.userViewId;
                app.globalData.userViewId = userViewId;
                console.log("3请求时发送的userViewId:" + userViewId);
                that.getSourceData();
              },
              fail: res => {
                that.$wuxToast.show({
                  type: 'text',
                  timer: 2500,
                  color: '#fff',
                  text: '获取信息失败，请返回首页后重试',
                  success: () => console.log('获取信息失败，请返回首页后重试')
                })
              }
            })
          } else {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '您还未授权获取您的用户信息，请退出重试！',
              success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
            })
          }
        },
        fail: function () {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: '无法获取授权信息，请退出重试',
            success: () => console.log('无法获取授权信息，请退出重试')
          })
        }
      });
    }
    that.count_down(); 
  },
  getSourceData:function(){
    wx.request({
      url: app.globalData.BaseURL + '/pt/' + that.data.ptInfoId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (datas) {
        util.hideToast();
        let ptData = JSON.parse(datas.data.data);
        console.log("/pt---res:" + ptData);
        that.setData({
          "ptData": ptData,
          "expireTime": ptData.ptInfo.expriedTime,
          "ptInfoId": ptData.ptInfo.id,
          "ptStatus": ptData.ptInfo.status,
          "ptFlag": ptData.flag,
          "ptId": ptData.pt.id,
          "ptSize": ptData.ptInfo.num
        })
        console.log("ptSize:" + that.data.ptSize);
        console.log("expireTime:" + that.data.expireTime);
        console.log("ptStatus:" + that.data.ptStatus);
        console.log("ptFlag:" + that.data.ptFlag);
        app.globalData.groupId = that.data.ptId;
        if (!ptData.flag) {
          app.globalData.ptInfoId = that.data.ptInfoId;
        }
      },
      fail:function(){
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败，请退出重试',
          success: () => console.log('请求失败，请退出重试')
        })
      }
    })
    if (that.data.fromViewUserId != null) {
      wx.request({
        url: app.globalData.BaseURL + '/ptrelative',
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
          "userViewId": app.globalData.userViewId
        },
        data: {
          "ptInfoId": that.data.ptInfoId,
          "fromViewUserId": that.data.fromViewUserId,
          "userViewId": app.globalData.userViewId,
        },
        success: function (datas) {
          util.hideToast();
          console.log('/ptrelative请求失败')
        },
        fail: function () {
          util.hideToast();
          console.log('/ptrelative请求失败')
        }
      })
    }
  },
  //邀请拼团
  onShareAppMessage: function (options) {
    var ptInfoId = that.data.ptInfoId;
    var fromViewUserId = app.globalData.userViewId;
    var imgUrl;
    if (that.data.ptSize == 2){//两人团
      imgUrl = "http://static.cakeboss.com.cn/kqbGroupBuy2.jpg";
    } else if (that.data.ptSize == 3){//三人团
      imgUrl = "http://static.cakeboss.com.cn/kqbGroupBuy3.jpg";
    } else if (that.data.ptSize == 8) {//三人团
      imgUrl = "http://static.cakeboss.com.cn/kqbGroupBuy8.jpg";
    }else{
      imgUrl = 'http://static.cakeboss.com.cn/kqbPTN1.jpg';
    }
    return {
      title: "您的好友邀您参与限时拼团，一起享受极净好空气！",
      path: '/pages/pintuan/index?ptInfoId=' + ptInfoId + '&fromViewUserId=' + fromViewUserId,
      imageUrl: imgUrl,
      success: function (options) {
        util.shareSuccessTost();
      },
      fail: function (options) {
        util.shareCancelTost();
      }
    }
  },
  /* 毫秒级倒计时 */
  count_down: function () {
    //2016-12-27 12:47:08 转换日期格式  
    var a = that.data.expireTime.split(/[^0-9]/);
    //截止日期：日期转毫秒  
    var expireMs = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
    //倒计时毫秒  
    var duringMs = expireMs.getTime() - (new Date()).getTime();
    // 渲染倒计时时钟  
    that.setData({
      clock: that.date_format(duringMs)
    });

    if (duringMs <= 0) {
      that.setData({
        clock: "00:00:00"
      });
      //加载提示框
      util.showLoading();
      //拼团超时，发送ptInfoId
      wx.request({
        url: app.globalData.BaseURL + '/pt/' + that.data.ptInfoId,
        method: 'POST',
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: function (res) {
          var code = res.data.code;
          if(code == 200) {
            that.setData({
              ptStatus: 3
            });
          }else if(code == 500){
            that.setData({
              ptStatus:2
            });
          }
          util.hideToast();
        }
      })
      // timeout则跳出递归  
      return;
    }
    setTimeout(function () {
      // 放在最后--  
      duringMs -= 10;
      that.count_down();
    }, 1000)
  },
  /* 格式化倒计时 */
  date_format: function (micro_second) {
    // 秒数  
    var second = Math.floor(micro_second / 1000);
    // 小时位   
    var hr = Math.floor(second / 3600);
    // 分钟位  
    var min = that.fill_zero_prefix(Math.floor((second - hr * 3600) / 60));
    // 秒位  
    var sec = that.fill_zero_prefix(second % 60);// equal to => var sec = second % 60;  
    return hr + ":" + min + ":" + sec + " ";
  },

  /* 分秒位数补0 */
  fill_zero_prefix: function (num) {
    return num < 10 ? "0" + num : num
  }  
})