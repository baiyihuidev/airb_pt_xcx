// pages/detail/detail.js
const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({
  data: {
    dataSource: {},
    // 商品规格数组
    goodsDtoList:[],
    // 选中商品规格数组
    chooseGoodsDtoList: [],
    // 轮播图相关设置
    swiperCurrent: 0,
    indicatorDots: true,  // 是否显示pages
    autoplay: true,//自动播放   
    interval: 2000,//间隔时间   
    duration: 1000,//监听滚动和点击事件  
    circular: true,
    // 上一页带过来的id
    id: 0,
    // 是否展示弹窗
    showModalStatus: false,
    // 价格
    payPrice: 0,
    imageWidth: wx.getSystemInfoSync().windowWidth,
    // 登录注册弹窗
    showDialog: false,
    //商品Id
    goodsId: "",
    //区分积分、拼团、亲友码
    tap: '',
    //用户授权信息
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo') ,
    couponId:"" ,
    avatarUrl: "http://static.cakeboss.com.cn/wx-tian-headerIcon.svg",
    nickName: "",//用户姓名
    oldGoodsId:"",//分享链接给老用户时，老用户点击进入的goodsId
    showSetting:false,
    totalNum:0, //选购的商品数量总和
  },
  onLoad: function (options) {
    that = this;
    console.log(options,"options");
    //加载提示框
    util.showLoading();
    this.$wuxToast = app.wux(this).$wuxToast;
    // var goodsId = options.goodsId;
    // var tap = options.tap;
    var goodsId = 5;
    var tap = 5;
    that.setData({
      tap: tap,
    })
    console.log("详情页tap:" + tap);
    console.log("详情页读取的goodsId:" + goodsId);
    
    app.globalData.tap = tap;
    if (goodsId != undefined) {
      that.setData({
        goodsId: goodsId
      }) 
      console.log("111goodsId:" + that.data.goodsId);
      if (app.globalData.userViewId) {
        that.setData({
          hasUserInfo: true
        });
        console.log("请求商品详情数据时发送的userViewId:" + app.globalData.userViewId);
        that.getSource();
      } else if (that.data.canIUse) {
        console.log("2请求商品详情数据时发送的userViewId之前：" + app.globalData.userViewId);
        // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
        // 所以此处加入 callback 以防止这种情况  
        app.userInfoReadyCallback = res => {
          var userInfo = JSON.parse(res.data.data);
          var userViewId = userInfo.userViewId;
          console.log(userInfo,"userInfo");
          that.setData({
            hasUserInfo: true
          });
          app.globalData.userViewId = userViewId;
          console.log("2请求商品详情数据时发送的userViewId:" + userViewId);
          that.getSource();
        }
      } else {
        console.log("3请求商品详情数据时发送的userViewId之前：" + app.globalData.userViewId);
        // 在没有 open-type=getUserInfo 版本的兼容处理  
        wx.getSetting({
          success: data => {
            console.log(data,"已经向用户请求过的权限");
            if (data.authSetting['scope.userInfo']) {
              wx.getUserInfo({
                success: res => {
                  var userInfo = JSON.parse(res.data.data);
                  var userViewId = userInfo.userViewId;
                  that.setData({
                    hasUserInfo: true
                  });
                  app.globalData.userViewId = userViewId;
                  console.log("3请求商品详情数据时发送的userViewId:" + userViewId);
                  that.getSource();
                },
                fail: res => {
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 2500,
                    color: '#fff',
                    text: '获取信息失败，请返回首页后重试',
                    success: () => console.log('获取信息失败，请返回首页后重试')
                  })
                }
              })
            } else {
              that.$wuxToast.show({
                type: 'text',
                timer: 2500,
                color: '#fff',
                text: '您还未授权获取您的用户信息，请点击授权！',
                success: () => console.log('您还未授权获取您的用户信息，请点击授权！')
              })
            }
          },
          fail: function () {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '无法获取授权信息，请退出重试',
              success: () => console.log('无法获取授权信息，请退出重试')
            })
          }
        });
      }
    }else{
      that.$wuxToast.show({
        type: 'text',
        timer: 2500,
        color: '#fff',
        text: '未获取到商品Id，请返回重试',
        success: () => console.log('未获取到商品Id，请返回重试')
      })
    }
    console.log("detailPageHasUserInfo:" + that.data.hasUserInfo);
  },
  onShow: function () {
    that.setData({
      showSetting: false
    })
  },
  // 分享
  // onShareAppMessage: function (event) {
  //   if (event.from === 'button') {
  //     var shareTitleStr = "我想购买空气堡新风机，求堡主赐个亲友码！";
  //     return {
  //       title: shareTitleStr,
  //       path: '/pages/detail/buyFriendCode/detail?goodsId=' + that.data.oldGoodsId,
  //       imageUrl: 'http://static.cakeboss.com.cn/kqbQinYouCodePostToOldUserN1.jpg',
  //       success: function (options) {
  //         util.shareSuccessTost();
  //       },
  //       fail: function (options) {
  //         util.shareCancelTost();
  //       }
  //     }
  //   }else{
  //     var shareTitleStr = "福利快戳！亲友码买空气堡最高直降千元！";
  //     return {
  //       title: shareTitleStr,
  //       path: '/pages/detail/friendCodeToBuy/detail?goodsId=' + that.data.goodsId,
  //       imageUrl: 'http://static.cakeboss.com.cn/kqbQinYouCodePostToNewUserN1.jpg',
  //       success: function (options) {
  //         util.shareSuccessTost();
  //       },
  //       fail: function (options) {
  //         util.shareCancelTost();
  //       }
  //     }
  //   }
  // },
  //请求数据
  getSource:function(e){
    console.log("请求页面数据时读取的goodsId："+that.data.goodsId);
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/detail?id=' + that.data.goodsId,
      // url: app.globalData.BaseURL + '/relativeCode/detail?id=5',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        if (res.data.code==200){
          var dataSource = res.data.data;
          console.log("dataSource========" + JSON.stringify(dataSource));
          util.hideToast();
          that.setData({
            dataSource: dataSource,
            goodsDtoList: dataSource.goodsDtos
          })
          app.globalData.cakeSpuItem = dataSource.spu;
        }else{
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: res.data.msg,
            success: () => console.log(res.data.msg)
          })
        }
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败，请退出重试',
          success: () => console.log('请求失败，请退出重试')
        })
      }
    })
  },
  //计算总价
  getTotalPrice: function (e) {
    let goodsDtoList = that.data.goodsDtoList;   // 获取购物车列表
    let total = 0;
    let chooseGoodsDtoList = [];
    for (let i = 0; i < goodsDtoList.length; i++) {    // 循环列表得到每个数据
      if (goodsDtoList[i].num > 0) {      // 判断选中才会计算价格
        let goodsDtoListGoods = new Object();
        
        //去除相乘时出现的17位小数点
        var n = 0,
          s1 = goodsDtoList[i].num.toString(),
          s2 = goodsDtoList[i].goods.payPrice.toString();
        try {
          n += s1.split(".")[1].length
        } catch (e) { }
        try {
          n += s2.split(".")[1].length
        } catch (e) { }
        let totalPrice = Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, n);
        total += totalPrice;// 所有价格加起来

        console.log("total==" + total);
        //去除相加时出现的17位小数点
        // if (total.toString().split(".")[1]) {
        //   if (total.toString().split(".")[1].length > 2) {
        //     total = total.toFixed(2);
        //   }
        // }


        goodsDtoListGoods.goodsId = goodsDtoList[i].goods.id;
        goodsDtoListGoods.num = goodsDtoList[i].num;
        chooseGoodsDtoList.push(goodsDtoListGoods);
      }
    }
    that.setData({   // 最后赋值到data中渲染到页面
      goodsDtoList: goodsDtoList,
      payPrice: total.toFixed(0),
      chooseGoodsDtoList: chooseGoodsDtoList
    });
    console.log("chooseGoodsDtoList:" + chooseGoodsDtoList);
  },
  //打开购物车弹窗
  showCartPopview(){
    that.setData({  
      showModalStatus: true
    });
  },
  //关闭购物车弹窗
  closeCartPopview() {
    this.setData({
      showModalStatus: false
    });
  },
  addCount: function (e) {
    var index = e.currentTarget.dataset.index;
    var goodsDtoList = that.data.goodsDtoList;
    var currentItem = goodsDtoList[index];
    currentItem.num++;
    that.data.totalNum++;
    goodsDtoList[index] = currentItem;
    that.setData({
      goodsDtoList: goodsDtoList
    });
    console.log("goodsDtoListNum:" + goodsDtoList[index].num);
    console.log(that.data.totalNum,"totalNum");
    that.getTotalPrice();
  },
  minusCount: function (e) {
    var index = e.currentTarget.dataset.index;
    if (that.data.goodsDtoList[index].num > 0) {
      var goodsDtoList = that.data.goodsDtoList;
      var currentItem = goodsDtoList[index];
      currentItem.num--;
      that.data.totalNum--;
      goodsDtoList[index] = currentItem;
      that.setData({
        goodsDtoList: goodsDtoList
      });
    }
    console.log(that.data.totalNum, "totalNum");
    that.getTotalPrice();
  },
  //传递参数，跳转创建订单页
  postChoose: function (e) {
    wx.request({
      url: app.globalData.BaseURL + '/activity/xcx/judgeActivityAddOrder',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: { 'num': that.data.totalNum },
      success: function (res) {
        if (res.data.code == 200){
          // let goodsId = that.data.goodsId;
          let chooseGoodsDtoList = JSON.stringify(that.data.chooseGoodsDtoList);
          app.globalData.chooseGoodsDtoList = chooseGoodsDtoList;
          console.log("chooseGoodsDtoList:" + typeof (chooseGoodsDtoList) + "====" + chooseGoodsDtoList);
          wx.navigateTo({
            url: '/pages/order/createOrder/groupActivityCreateOrder/groupActivityCreateOrder?num=' + that.data.totalNum +'&tap='+that.data.tap,
          })
        }else{
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: res.data.msg,
            success: () => console.log(res.data.msg)
          })
        }
      },
      fail: function (res) {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误，请重试~',
          success: () => console.log('请求错误，请重试~')
        })
      }
    })  
  },
  //没有授权，请求用户授权
  bindGetUserInfo: function (e) {
    console.log("userInfo:" + e.detail.userInfo);
    var userInfo = e.detail.userInfo;
    var userInfoData = JSON.stringify(e.detail);
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          app.globalData.hasUserInfo = true;
          app.globalData.userInfo = userInfo;
          app.globalData.avatarUrl = userInfo.avatarUrl;
          console.log("productDetailPageUserInfo:" + userInfoData + "type:" + typeof (userInfoData));
          wx.request({
            url: app.globalData.BaseURL + '/wx/userinfo',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: { 'data': userInfoData },
            success: function (res) {
              var unionId = res.data.data;
              app.globalData.unionId = unionId;
              that.postChoose();
              console.log("productDetailPageUnionId:" + unionId);
            },
            fail: function (res) {
              console.log("获取信息失败");
              that.globalData.openId = "";
            }
          })
        } else {
          return;
        }
      }
    })
  },
  //已经授权，确认付款获取用户信息，提交选择
  showDialogBtn: function (e) {
    let payPrice = that.data.payPrice;
    if (payPrice != 0) {
      that.postChoose();
    } else {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请选择商品',
        success: () => console.log('请选择商品')
      });
    }
  }
})
