// pages/detail/detail.js
const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({
  data: {
    dataSource: {},
    // 商品规格数组
    goodsDtoList: [],
    // 选中商品规格数组
    chooseGoodsDtoList: [],
    // 轮播图相关设置
    swiperCurrent: 0,
    indicatorDots: true,  // 是否显示pages
    autoplay: true,//自动播放   
    interval: 2000,//间隔时间   
    duration: 1000,//监听滚动和点击事件  
    circular: true,
    // 上一页带过来的id
    id: 0,
    // 是否展示弹窗
    showModalStatus: false,
    // 价格
    payPrice:0,
    imageWidth: wx.getSystemInfoSync().windowWidth,
    // 登录注册弹窗
    showDialog: false,
    //商品Id
    groupId: "",
    ptInfoId:null,
    //区分积分、拼团、亲友码
    tap:2,
    //用户授权信息
    hasUserInfo:"",
    num:'',//拼团人数
  },
  onLoad: function (options) {
    console.log("详情：", options);
    that = this;
    //加载提示框
    util.showLoading();
    this.$wuxToast = app.wux(this).$wuxToast;
    var pintuanPage = options.pintuanPage;
    var num = options.num;
    
    //设置拼团人数，用于分享图片显示是不同人数的拼团
    if (num){
      that.setData({
        num: num,
      })
    }
    console.log("pintuanPage:" + pintuanPage);
    if (pintuanPage==1){//从拼团页跳转进来的
      that.setData({
        groupId: app.globalData.groupId,
        ptInfoId: app.globalData.ptInfoId,
        payPrice: that.data.payPrice,
        hasUserInfo: app.globalData.hasUserInfo
      })
    } else if (options.q) { //判断是扫码进入
      that.chargeCodeEnter(options);
    } else {//从首页跳转进来的
      that.setData({
        groupId: options.groupId,
        payPrice: that.data.payPrice,
        hasUserInfo: app.globalData.hasUserInfo
      })
    }
    console.log("detailPageHasUserInfo:" + that.data.hasUserInfo)
    var urlStr = app.globalData.BaseURL + '/spu/pt/' + that.data.groupId;
    var parameters = "";
    console.log("groupId:" + that.data.groupId);
    wx.request({
      url: urlStr, 
      method: 'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // var dataDictionary = JSON.parse(res.data.data);
        var dataDictionary = res.data.data;
        util.hideToast();
        that.setData({
          dataSource: dataDictionary,
          goodsDtoList: dataDictionary.goodsDtoList
        })
        app.globalData.cakeSpuItem = dataDictionary.spu;
      },
      fail:function(){
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  // 判断是否是扫描二维码进入
  chargeCodeEnter: function (options) {
    var url = decodeURIComponent(options.q);
    console.log(url, "url");
    // var groupId = q.groupId;
    var groupId = util.GetUrlParam(url, 'groupId');
    var tap = util.GetUrlParam(url, 'tap');
    var num = util.GetUrlParam(url, 'num');
    console.log(groupId, "groupId", tap, "tap", num, "num");
    that.setData({
      groupId: groupId,
      tap: tap,
      num: num
    })
  },
  // 分享
  onShareAppMessage: function (options) {
    var shareTitleStr = "您的好友邀您参与限时拼团，一起享受极净好空气！";
    var imageUrl;
    if (that.data.num == 2) {
      imageUrl = "http://static.cakeboss.com.cn/kqbGroupBuy2.jpg";
    } else if (that.data.num == 3) {
      imageUrl = "http://static.cakeboss.com.cn/kqbGroupBuy3.jpg";
    } else if (that.data.num == 8) {
      imageUrl = "http://static.cakeboss.com.cn/kqbGroupBuy8.jpg";
    } else {
      imageUrl = 'http://static.cakeboss.com.cn/kqbPTN1.jpg';
    }
    console.log(that.data.num, "num", imageUrl, "imageUrl");
    return {
      title: shareTitleStr,
      path: '/pages/detail/groupDetail/detail?groupId=' + that.data.groupId+'&tap=2',
      imageUrl: imageUrl,
      success: function (options) {
        util.shareSuccessTost();
      },
      fail: function (options) {
        util.shareCancelTost();
      }
    }
  },
  //计算总价
  getTotalPrice:function(e){
    let goodsDtoList = that.data.goodsDtoList;   // 获取购物车列表
    let total = 0;   
    let chooseGoodsDtoList=[];
    for (let i = 0; i < goodsDtoList.length; i++) {    // 循环列表得到每个数据
      if (goodsDtoList[i].num>0) {      // 判断选中才会计算价格
        let goodsDtoListGoods = new Object();
        
        //去除相乘时出现的17位小数点
        var n = 0, 
            s1 = goodsDtoList[i].num.toString(),
            s2 = goodsDtoList[i].goods.payPrice.toString();
        try {
          n += s1.split(".")[1].length
        } catch (e) { }
        try {
          n += s2.split(".")[1].length
        } catch (e) { }
        let totalPrice = Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, n);
        total += totalPrice;// 所有价格加起来

        console.log("total=="+total);
        //去除相加时出现的17位小数点
        // if (total.toString().split(".")[1]){
        //   if (total.toString().split(".")[1].length > 2){
        //     total=total.toFixed(1);
        //   }
        // }
        
        goodsDtoListGoods.goodsId = goodsDtoList[i].goods.id;
        goodsDtoListGoods.num = goodsDtoList[i].num;
        chooseGoodsDtoList.push(goodsDtoListGoods);

      }
    }
    that.setData({   // 最后赋值到data中渲染到页面
      goodsDtoList: goodsDtoList,
      payPrice: total.toFixed(0),
      chooseGoodsDtoList: chooseGoodsDtoList
    }); 
    console.log("chooseGoodsDtoList:" + chooseGoodsDtoList);
  },
  showCartPopview(){
    this.setData({  
      showModalStatus: true
    });
  },
  closeCartPopview() {
    this.setData({
      showModalStatus: false
    });
  },
  addCount: function (e){
    var index = e.currentTarget.dataset.index;
    var goodsDtoList = that.data.goodsDtoList;
    var currentItem = goodsDtoList[index];
    currentItem.num++;
    goodsDtoList[index] = currentItem;
    that.setData({ 
      goodsDtoList: goodsDtoList
    });
    that.getTotalPrice();
  },
  minusCount: function (e){
    var index = e.currentTarget.dataset.index;
    if (that.data.goodsDtoList[index].num > 0) {
      var goodsDtoList = that.data.goodsDtoList;
      var currentItem = goodsDtoList[index];
      currentItem.num--;
      goodsDtoList[index] = currentItem;
      that.setData({
        goodsDtoList: goodsDtoList
      });
    }
    that.getTotalPrice();
  },
  //传递参数，跳转创建订单页
  postChoose:function (e) {
    let groupId = that.data.groupId;
    let chooseGoodsDtoList = JSON.stringify(that.data.chooseGoodsDtoList);
    app.globalData.chooseGoodsDtoList = chooseGoodsDtoList;
    app.globalData.groupId = groupId;
    app.globalData.tap = 2;
    console.log("chooseGoodsDtoList:" + typeof (chooseGoodsDtoList) + "====" + chooseGoodsDtoList);
    wx.navigateTo({
      url: '/pages/order/createOrder/createOrder',
    })
  },
  //没有授权，请求用户授权
  bindGetUserInfo: function (e) {
    console.log("userInfo:"+e.detail.userInfo);
    var userInfo = e.detail.userInfo;
    var userInfoData = JSON.stringify(e.detail);
    wx.getSetting({ 
      success(res) {  
        if (res.authSetting['scope.userInfo']) {
          app.globalData.hasUserInfo = true;
          app.globalData.userInfo = userInfo;
          app.globalData.avatarUrl = userInfo.avatarUrl;
          console.log("productDetailPageUserInfo:" + userInfoData + "type:" + typeof (userInfoData));
          wx.request({
            url: app.globalData.BaseURL + '/wx/userinfo',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: { 'data': userInfoData },
            success: function (res) {
              var unionId = res.data.data;
              app.globalData.unionId = unionId;
              that.postChoose();
              console.log("productDetailPageUnionId:" + unionId);
            }, 
            fail: function (res) {
              console.log("获取信息失败");
              that.globalData.openId = "";
            }
          }) 
        } else {
          return;
        }
      }
    })
  },
  //已经授权，确认付款获取用户信息，提交选择
  showDialogBtn: function (e){
    let payPrice = that.data.payPrice;
    if (payPrice!=0){
      that.postChoose();
    }else{
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请选择商品',
        success: () => console.log('请选择商品')
      });
    }
  }
})
