// pages/detail/detail.js
const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({
  data: {
    dataSource: {},
    // 轮播图相关设置
    swiperCurrent: 0,
    indicatorDots: true,  // 是否显示pages
    autoplay: true,//自动播放   
    interval: 2000,//间隔时间   
    duration: 1000,//监听滚动和点击事件  
    circular: true,
    // 上一页带过来的id
    id: 0,
    imageWidth: wx.getSystemInfoSync().windowWidth,
    // 登录注册弹窗
    showDialog: false,
    //商品Id
    goodsId: "",
    //区分积分、拼团、亲友码
    tap: 3,
    //用户授权信息
    hasUserInfo: ""
  },
  onLoad: function (options) {
    that = this;
    //加载提示框
    util.showLoading();
    this.$wuxToast = app.wux(this).$wuxToast;
    that.setData({
      goodsId: options.goodsId,
      hasUserInfo: app.globalData.hasUserInfo
    })
    console.log("detailPageHasUserInfo:" + that.data.hasUserInfo);
    console.log("detailPagePointSpuId:" + options.spuId);
    var urlStr = app.globalData.BaseURL + '/integral/detail?id=' + that.data.goodsId;
    wx.request({
      url: urlStr,
      method: 'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // var dataDictionary = JSON.parse(res.data.data);
        var dataDictionary = res.data;
        util.hideToast();
        that.setData({
          dataSource: dataDictionary
        })
        app.globalData.cakeSpuItem = dataDictionary.spu;
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  //传递参数，跳转创建订单页
  postChoose: function (e) {
    let chooseGoodsDtoList = JSON.stringify(that.data.dataSource);
    app.globalData.chooseGoodsDtoList = chooseGoodsDtoList;
    app.globalData.tap = 3;
    app.globalData.goodsId = that.data.goodsId
    console.log("chooseGoodsDtoList:" + typeof (chooseGoodsDtoList) + "====" + chooseGoodsDtoList);
    util.showLoading();
    console.log("detailPageUserViewId:" + app.globalData.userViewId);
    console.log("detailPage存储的全局GoodsId:" + app.globalData.goodsId);
    wx.request({
      url: app.globalData.BaseURL + '/integral/redeem',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data: { 'id': that.data.goodsId},
      success: function (res) {
        util.hideToast();
        console.log("detailResDataCode:"+res.data.code);
        if (res.data.code == 200){
          wx.navigateTo({
            url: '/pages/order/createOrder/pointCreateOrder/pointCreateOrder',
          })
        }else{
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: res.data.msg
          });
          console.log(res.data.msg);
          return;
        }
      },
      fail: function (res) {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  //没有授权，请求用户授权
  bindGetUserInfo: function (e) {
    console.log("userInfo:" + e.detail.userInfo);
    var userInfo = e.detail.userInfo;
    var userInfoData = JSON.stringify(e.detail);
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          app.globalData.hasUserInfo = true;
          app.globalData.userInfo = userInfo;
          app.globalData.avatarUrl = userInfo.avatarUrl;
          console.log("productDetailPageUserInfo:" + userInfoData + "type:" + typeof (userInfoData));
          util.showLoading();
          wx.request({
            url: app.globalData.BaseURL + '/wx/userinfo',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: { 'data': userInfoData },
            success: function (res) {
              util.hideToast();
              var unionId = res.data.data;
              app.globalData.unionId = unionId;
              that.postChoose();
              console.log("productDetailPageUnionId:" + unionId);
            },
            fail: function (res) {
              util.hideToast();
              console.log("获取信息失败");
              that.globalData.openId = "";
            }
          })
        } else {
          return;
        }
      }
    })
  },
  //已经授权，确认付款获取用户信息，提交选择
  showDialogBtn: function (e) {
    that.postChoose();
  },
  // 分享
  onShareAppMessage: function (options) {
    var shareTitleStr = "积分就能换滤网！快来发现更多好物！";
    return {
      title: shareTitleStr,
      path: '/pages/detail/pointDetail/detail?goodsId=' + that.data.goodsId+'?tap=3',
      // imageUrl: 'http://static.cakeboss.com.cn/kqbGQYM.jpg',
      success: function (options) {
        util.shareSuccessTost();
      },
      fail: function (options) {
        util.shareCancelTost();
      }
    }
  },
})
