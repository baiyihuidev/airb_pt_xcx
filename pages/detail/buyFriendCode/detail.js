// pages/detail/detail.js
const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({
  data: {
    dataSource: {},
    //商品Id
    goodsId: "",
    // 轮播图相关设置
    swiperCurrent: 0,
    indicatorDots: true,  // 是否显示pages
    autoplay: true,//自动播放   
    interval: 2000,//间隔时间   
    duration: 1000,//监听滚动和点击事件  
    circular: true,
    //用户授权信息
    hasUserInfo: "",
    imageWidth: wx.getSystemInfoSync().windowWidth,
    canIUse: wx.canIUse('button.open-type.getUserInfo')  
  },
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
    let goodsId = options.goodsId;
    that.setData({
      goodsId: options.goodsId,
      hasUserInfo: app.globalData.hasUserInfo
    })
    util.showLoading();
    if (app.globalData.userViewId) {
      console.log("1请求时发送的userViewId:" + app.globalData.userViewId);
      that.getSourceData();
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
      // 所以此处加入 callback 以防止这种情况  
      app.userInfoReadyCallback = res => {
        var userInfo = JSON.parse(res.data.data);
        var userViewId = userInfo.userViewId;
        app.globalData.userViewId = userViewId;
        console.log("2请求时发送的userViewId:" + userViewId);
        that.getSourceData();
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理  
      wx.getSetting({
        success: data => {
          if (data.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                var userInfo = JSON.parse(res.data.data);
                var userViewId = userInfo.userViewId;
                app.globalData.userViewId = userViewId;
                console.log("3请求时发送的userViewId:" + userViewId);
                that.getSourceData();
              },
              fail: res => {
                that.$wuxToast.show({
                  type: 'text',
                  timer: 2500,
                  color: '#fff',
                  text: '获取信息失败，请返回首页后重试',
                  success: () => console.log('获取信息失败，请返回首页后重试')
                })
              }
            })
          } else {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '您还未授权获取您的用户信息，请退出重试！',
              success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
            })
          }
        },
        fail: function () {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: '无法获取授权信息，请退出重试',
            success: () => console.log('无法获取授权信息，请退出重试')
          })
        }
      });
    }
  },
  //请求数据
  getSourceData:function(){
    console.log("goodsId:" + that.data.goodsId)
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/detail?id=' + that.data.goodsId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        util.hideToast();
        var code = res.data.code;
        console.log("购亲友码详情页数据：" + that.data.dataSource);
        that.setData({
          dataSource: res.data.data
        })
      },
      fail:function(){
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误，请重新请求',
        })
      }
    })
  },
  //数据提交
  //传递参数，跳转创建订单页
  postChoose: function (e) {
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/detail?id=' + that.data.goodsId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        util.hideToast();
        console.log("获取详情data：", res.data);
        console.log("获取详情code：", res.code);
        if (res.data.code==200){
          let goodsDtoListGoods = new Object();
          let chooseGoodsDtoList = [];
          // let dataSource = JSON.stringify(that.data.dataSource);
          goodsDtoListGoods.goodsId = that.data.goodsId;
          goodsDtoListGoods.num = 1;
          chooseGoodsDtoList.push(goodsDtoListGoods);
          app.globalData.chooseGoodsDtoList = JSON.stringify(chooseGoodsDtoList);
          app.globalData.tap = 1;
          console.log("chooseGoodsDtoList:" + typeof (app.globalData.chooseGoodsDtoList) + "====" + app.globalData.chooseGoodsDtoList);
          wx.navigateTo({
            url: '/pages/order/createOrder/buyCodeCreateOrder/buyCodeCreateOrder',
          })
        }else{
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: res.data.msg,
            success: () => {
              return;
            }
          })
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误，请重新请求',
          success: () => wx.switchTab({
            url: '/pages/index/index'
          })
        })
      }
    })
  },
  //没有授权，请求用户授权
  bindGetUserInfo: function (e) {
    console.log("userInfo:" + e.detail.userInfo);
    var userInfo = e.detail.userInfo;
    var userInfoData = JSON.stringify(e.detail);
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          app.globalData.hasUserInfo = true;
          app.globalData.userInfo = userInfo;
          app.globalData.avatarUrl = userInfo.avatarUrl;
          console.log("productDetailPageUserInfo:" + userInfoData + "type:" + typeof (userInfoData));
          wx.request({
            url: app.globalData.BaseURL + '/wx/userinfo',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: { 'data': userInfoData },
            success: function (res) {
              console.log("请求领亲友码绑定老用户：", res.data.data);
              var unionId = res.data.data;
              app.globalData.unionId = unionId;
              that.postChoose();
              console.log("productDetailPageUnionId:" + unionId);
            },
            fail: function (res) {
              console.log("获取信息失败");
              that.globalData.openId = "";
            }
          })
        } else {
          return;
        }
      }
    })
  }
})
