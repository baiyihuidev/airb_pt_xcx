// pages/detail/detail.js
const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({
  data: {
    dataSource: {},
    // 商品规格数组
    goodsDtoList:[],
    // 选中商品规格数组
    chooseGoodsDtoList: [],
    // 轮播图相关设置
    swiperCurrent: 0,
    indicatorDots: true,  // 是否显示pages
    autoplay: true,//自动播放   
    interval: 2000,//间隔时间   
    duration: 1000,//监听滚动和点击事件  
    circular: true,
    // 上一页带过来的id
    id: 0,
    // 是否展示弹窗
    showModalStatus: false,
    // 价格
    payPrice: 0,
    imageWidth: wx.getSystemInfoSync().windowWidth,
    // 登录注册弹窗
    showDialog: false,
    //商品Id
    goodsId: "",
    //区分积分、拼团、亲友码
    tap: '',
    //用户授权信息
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo') ,
    couponId:"" ,
    avatarUrl: "http://static.cakeboss.com.cn/wx-tian-headerIcon.svg",
    nickName: "",//用户姓名
    codeShareImg:"",//生成的带二维码的分享图
    oldGoodsId:"",//分享链接给老用户时，老用户点击进入的goodsId
    showSetting:false
  },
  onLoad: function (options) {
    that = this;
    //加载提示框
    util.showLoading();
    this.$wuxToast = app.wux(this).$wuxToast;
    var goodsId = options.goodsId;
    // var goodsId = "";
    var tap = options.tap;
    var couponId = options.couponId;
    // var couponId = 120;
    var userInfo = options.userInfo;
    console.log("详情页tap:" + tap);
    console.log("详情页读取的CouponId:" + options.couponId);
    that.setData({
      // payPrice: that.data.payPrice,
      hasUserInfo: app.globalData.hasUserInfo,
      tap: tap,
    })
    app.globalData.tap = tap;
    console.log(goodsId, "goodsId=====", userInfo, "userInfo", couponId, "couponId", that.data.hasUserInfo,"hasUserInfo");
    if (goodsId && app.globalData.userViewId) {//从首页跳转进来的
      that.setData({
        goodsId: goodsId
      })
      that.getSource();
    } else if (!couponId && !goodsId){//从分享商品详情页进来的
      if (app.globalData.userInfo) {
        that.setData({
          hasUserInfo: true
        });
        console.log("1请求时发送的userViewId:" + app.globalData.userViewId);
        that.getSource();
      } else if (that.data.canIUse) {
        // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
        // 所以此处加入 callback 以防止这种情况  
        app.userInfoReadyCallback = res => {
          var userInfo = JSON.parse(res.data.data);
          var userViewId = userInfo.userViewId;
          that.setData({
            hasUserInfo: true
          });
          app.globalData.userViewId = userViewId;
          console.log("2请求数据时发送的userViewId:" + userViewId);
          that.getSource();
        }
      } else {
        // 在没有 open-type=getUserInfo 版本的兼容处理  
        wx.getSetting({
          success: data => {
            if (data.authSetting['scope.userInfo']) {
              wx.getUserInfo({
                success: res => {
                  var userInfo = JSON.parse(res.data.data);
                  var userViewId = userInfo.userViewId;
                  that.setData({
                    hasUserInfo: true
                  });
                  app.globalData.userViewId = userViewId;
                  console.log("3请求时发送的userViewId:" + userViewId);
                  that.getSource();
                },
                fail: res => {
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 2500,
                    color: '#fff',
                    text: '获取信息失败，请返回首页后重试',
                    success: () => console.log('获取信息失败，请返回首页后重试')
                  })
                }
              })
            } else {
              that.$wuxToast.show({
                type: 'text',
                timer: 2500,
                color: '#fff',
                text: '您还未授权获取您的用户信息，请退出重试！',
                success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
              })
            }
          },
          fail: function () {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '无法获取授权信息，请退出重试',
              success: () => console.log('无法获取授权信息，请退出重试')
            })
          }
        });
      }
    } else if (couponId && !userInfo) {//从亲友码分享链接跳转进来的
      that.setData({
        couponId: couponId
      })
      if (app.globalData.userInfo) {
        that.setData({
          hasUserInfo: true
        });
        console.log("1首页/relativeCode/receiveRelativeCode请求时发送的userViewId:" + app.globalData.userViewId);
        that.bindingFriendCoupon();
      } else if (that.data.canIUse) {
        // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回  
        // 所以此处加入 callback 以防止这种情况  
        app.userInfoReadyCallback = res => {
          var userInfo = JSON.parse(res.data.data);
          var userViewId = userInfo.userViewId;
          that.setData({
            hasUserInfo: true
          });
          app.globalData.userViewId = userViewId;
          console.log("2首页/relativeCode/receiveRelativeCode请求时发送的userViewId:" + userViewId);
          that.bindingFriendCoupon();
        }
      } else {
        // 在没有 open-type=getUserInfo 版本的兼容处理  
        wx.getSetting({
          success: data => {
            if (data.authSetting['scope.userInfo']) {
              wx.getUserInfo({
                success: res => {
                  var userInfo = JSON.parse(res.data.data);
                  var userViewId = userInfo.userViewId;
                  that.setData({
                    hasUserInfo: true
                  });
                  app.globalData.userViewId = userViewId;
                  console.log("3首页/relativeCode/receiveRelativeCode请求时发送的userViewId:" + userViewId);
                  that.bindingFriendCoupon();
                },
                fail: res => {
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 2500,
                    color: '#fff',
                    text: '获取信息失败，请返回首页后重试',
                    success: () => console.log('获取信息失败，请返回首页后重试')
                  })
                }
              })
            }else{
              that.$wuxToast.show({
                type: 'text',
                timer: 2500,
                color: '#fff',
                text: '您还未授权获取您的用户信息，请退出重试！',
                success: () => console.log('您还未授权获取您的用户信息，请退出重试！')
              })
            }
          },
          fail: function () {
            that.$wuxToast.show({
              type: 'text',
              timer: 2500,
              color: '#fff',
              text: '无法获取授权信息，请退出重试',
              success: () => console.log('无法获取授权信息，请退出重试')
            })
          }
        });
      }
    } else if (userInfo){//从我领取的亲友码列表点击“去使用”，跳转进来的
      wx.request({//请求goodId
        url: app.globalData.BaseURL + '/relativeCode/getNewRelativeCode',
        method: "GET",
        header: {
          'content-type': 'application/json', // 默认值
          "userViewId": app.globalData.userViewId
        },
        success: function (source) {
          console.log("self请求回来的goodsId--msg:" + JSON.stringify(source.data));
          console.log("self请求回来的goodsId:" + source.data.data);
          var goodsId = source.data.data;
          that.setData({
            goodsId: goodsId
          })
          that.getSource();
        }
      })
    }
    console.log("detailPageHasUserInfo:" + that.data.hasUserInfo);
    console.log("goodsId:" + that.data.goodsId);
  },
  onShow: function () {
    that.setData({
      showSetting: false
    }) 
  },
  // 分享
  onShareAppMessage: function (event) {
    if (event.from === 'button') {
      var shareTitleStr = "我想购买空气堡新风机，求堡主赐个亲友码！";
      return {
        title: shareTitleStr,
        path: '/pages/detail/buyFriendCode/detail?goodsId=' + that.data.oldGoodsId,
        imageUrl: 'http://static.cakeboss.com.cn/kqbQinYouCodePostToOldUserN1.jpg',
        success: function (options) {
          util.shareSuccessTost();
        },
        fail: function (options) {
          util.shareCancelTost();
        }
      }
    }else{
      console.log(that.data.goodsId,"share的goodsId");
      var shareTitleStr = "福利快戳！亲友码买空气堡最高直降千元！";
      return {
        title: shareTitleStr,
        path: '/pages/detail/friendCodeToBuy/detail?goodsId=' + that.data.goodsId,
        imageUrl: 'http://www.qiniu.airburgzen.cn/%E6%96%B0%E7%94%A8%E6%88%B7%E4%BD%BF%E7%94%A8%E4%BA%B2%E5%8F%8B%E7%A0%81%E8%B4%AD%E6%9C%BA111.jpg',
        success: function (options) {
          util.shareSuccessTost();
        },
        fail: function (options) {
          util.shareCancelTost();
        }
      }
    }
  },
  //绑定亲友码
  bindingFriendCoupon: function () {
    var couponId = that.data.couponId;
    console.log("领取亲友码请求时发送的CouponId:" + couponId);
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/receiveRelativeCode?relativeCodeInfoId=' + couponId,
      method: "GET",
      header: {
        'content-type': 'application/json', // 默认值
        "userViewId": app.globalData.userViewId
      },
      success: function (source) {
        console.log("请求回来的goodsId--msg:" + JSON.stringify(source.data));
        console.log("请求回来的goodsId:" + source.data.data);
        util.hideToast();
        var goodsId = source.data.data.goodsId;
        that.setData({
          goodsId: goodsId
        })
        if (source.data.code == 200) {
          that.getSource();
          wx.showModal({
            title: '领取成功',
            content: '好友送了您一个价值￥' + source.data.data.reducePrice.split("-")[1]+'的亲友码',
            confirmColor: '#00B0EB',
            cancelText: "个人中心",
            confirmText: "立即使用",
            success: function (res) {
              if (res.cancel) {
                wx.redirectTo({
                  url: '/pages/coupon/allList/allList?tap=4',
                })
              }else{
                that.setData({
                  showModalStatus: true
                });
              }
            }
          });
        } else {
          that.$wuxToast.show({
            type: 'text',
            timer: 2500,
            color: '#fff',
            text: source.data.msg,
            success: () => console.log(source.data.msg)
          })
          setTimeout(function () {
            that.getSource();
          }, 2500);
        }
      }
    })
  },
  //请求数据
  getSource:function(e){
    console.log("请求页面数据时读取的goodsId："+that.data.goodsId);
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/detail?id=' + that.data.goodsId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        if (res.data.code==200){
          var dataSource = res.data.data;
          console.log("dataSource========" + JSON.stringify(dataSource));
          util.hideToast();
          that.setData({
            dataSource: dataSource,
            goodsDtoList: dataSource.goodsDtos
          })
          app.globalData.cakeSpuItem = dataSource.spu;
        }else{
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: res.data.msg,
            success: () => console.log(res.data.msg)
          })
        }
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败，请退出重试',
          success: () => console.log('请求失败，请退出重试')
        })
      }
    })
  },
  //计算总价
  getTotalPrice: function (e) {
    let goodsDtoList = that.data.goodsDtoList;   // 获取购物车列表
    let total = 0;
    let chooseGoodsDtoList = [];
    for (let i = 0; i < goodsDtoList.length; i++) {    // 循环列表得到每个数据
      if (goodsDtoList[i].num > 0) {      // 判断选中才会计算价格
        let goodsDtoListGoods = new Object();
        
        //去除相乘时出现的17位小数点
        var n = 0,
          s1 = goodsDtoList[i].num.toString(),
          s2 = goodsDtoList[i].goods.payPrice.toString();
        try {
          n += s1.split(".")[1].length
        } catch (e) { }
        try {
          n += s2.split(".")[1].length
        } catch (e) { }
        let totalPrice = Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, n);
        total += totalPrice;// 所有价格加起来

        console.log("total==" + total);
        //去除相加时出现的17位小数点
        // if (total.toString().split(".")[1]) {
        //   if (total.toString().split(".")[1].length > 2) {
        //     total = total.toFixed(2);
        //   }
        // }


        goodsDtoListGoods.goodsId = goodsDtoList[i].goods.id;
        goodsDtoListGoods.num = goodsDtoList[i].num;
        chooseGoodsDtoList.push(goodsDtoListGoods);
      }
    }
    that.setData({   // 最后赋值到data中渲染到页面
      goodsDtoList: goodsDtoList,
      payPrice: total.toFixed(0),
      chooseGoodsDtoList: chooseGoodsDtoList
    });
    console.log("chooseGoodsDtoList:" + chooseGoodsDtoList);
  },
  //打开购物车弹窗
  showCartPopview(){
    that.setData({  
      showModalStatus: true
    });
  },
  //打开锁码弹窗
  showRequestPop() {
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/getOldRelativeCode',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code;
        if (code == 200) {
          util.hideToast();
          that.setData({
            showRequestPopModalStatus: true,
            oldGoodsId: res.data.data
          });
          console.log(that.data.oldGoodsId);
        } else {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '请求失败',
            success: () => console.log('请求失败')
          })
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  //关闭购物车弹窗
  closeCartPopview() {
    this.setData({
      showModalStatus: false
    });
  },
  //关闭锁码弹窗
  closeRequestPopview() {
    this.setData({
      showRequestPopModalStatus: false 
    });
  }, 
  //关闭生成的二维码弹窗
  closeCodePopview() {
    this.setData({
      showCodePopModalStatus: false
    });
  },
  addCount: function (e) {
    var index = e.currentTarget.dataset.index;
    var goodsDtoList = that.data.goodsDtoList;
    var currentItem = goodsDtoList[index];
    currentItem.num++;
    goodsDtoList[index] = currentItem;
    that.setData({
      goodsDtoList: goodsDtoList
    });
    console.log("goodsDtoListNum:" + goodsDtoList[index].num);
    that.getTotalPrice();
  },
  minusCount: function (e) {
    var index = e.currentTarget.dataset.index;
    if (that.data.goodsDtoList[index].num > 0) {
      var goodsDtoList = that.data.goodsDtoList;
      var currentItem = goodsDtoList[index];
      currentItem.num--;
      goodsDtoList[index] = currentItem;
      that.setData({
        goodsDtoList: goodsDtoList
      });
    }
    that.getTotalPrice();
  },
  //传递参数，跳转创建订单页
  postChoose: function (e) {
    // let goodsId = that.data.goodsId;
    let chooseGoodsDtoList = JSON.stringify(that.data.chooseGoodsDtoList);
    app.globalData.chooseGoodsDtoList = chooseGoodsDtoList;
    console.log("chooseGoodsDtoList:" + typeof (chooseGoodsDtoList) + "====" + chooseGoodsDtoList);
    wx.navigateTo({
      url: '/pages/order/createOrder/codeToBuyCreateOrder/codeToBuyCreateOrder',
    })
  },
  //没有授权，请求用户授权
  bindGetUserInfo: function (e) {
    console.log("userInfo:" + e.detail.userInfo);
    var userInfo = e.detail.userInfo;
    var userInfoData = JSON.stringify(e.detail);
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          app.globalData.hasUserInfo = true;
          app.globalData.userInfo = userInfo;
          app.globalData.avatarUrl = userInfo.avatarUrl;
          console.log("productDetailPageUserInfo:" + userInfoData + "type:" + typeof (userInfoData));
          wx.request({
            url: app.globalData.BaseURL + '/wx/userinfo',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
              "userViewId": app.globalData.userViewId
            },
            data: { 'data': userInfoData },
            success: function (res) {
              var unionId = res.data.data;
              app.globalData.unionId = unionId;
              that.postChoose();
              console.log("productDetailPageUnionId:" + unionId);
            },
            fail: function (res) {
              console.log("获取信息失败");
              that.globalData.openId = "";
            }
          })
        } else {
          return;
        }
      }
    })
  },
  //已经授权，确认付款获取用户信息，提交选择
  showDialogBtn: function (e) {
    let payPrice = that.data.payPrice;
    if (payPrice != 0) {
      that.postChoose();
    } else {
      that.$wuxToast.show({
        type: 'text',
        timer: 1500,
        color: '#fff',
        text: '请选择商品',
        success: () => console.log('请选择商品')
      });
    }
  },
  createCode: function (e) {
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/distributePicture',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data:{
        relativeCodeId: 1
      },
      success: function (res) {
        util.hideToast();
        if (res.data.code==200){
          console.log(res.data.data);
          that.setData({
            showRequestPopModalStatus:false,
            showCodePopModalStatus:true,
            codeShareImg: res.data.data
          })
          console.log("生成的二维码图片codeShareImg:" + that.data.codeShareImg);
        }else{
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '请求错误',
            success: () => console.log('请求错误')
          })
        }
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求错误',
          success: () => console.log('请求错误')
        })
      }
    })
  },
  //保存图片
  saveImgToPhotosAlbumTap: function () {
    util.showLoading();
    wx.getImageInfo({
      src: that.data.codeShareImg,
      success: function (res) {
        var path= res.path;
        console.log("path:"+path)
        wx.getSetting({
          success(res) {
            if (!res.authSetting['scope.writePhotosAlbum']) {
              wx.authorize({
                scope: 'scope.writePhotosAlbum',
                success() {
                  wx.saveImageToPhotosAlbum({
                    filePath: path,
                    success(result) {
                      console.log(result)
                      that.setData({
                        showRequestPopModalStatus: false,
                        showCodePopModalStatus: false
                      });
                      util.hideToast();
                      that.$wuxToast.show({
                        type: 'text',
                        timer: 1500,
                        color: '#fff',
                        text: '保存成功',
                        success: () => console.log('保存成功')
                      })
                    },
                    fail: function (fail) {
                      util.hideToast();
                      var fails = JSON.stringify(fail);
                      console.log("保存失败:" + fails);
                      that.$wuxToast.show({
                        type: 'text',
                        timer: 1500,
                        color: '#fff',
                        text: '保存失败',
                        success: () => console.log('保存失败')
                      })
                    }
                  })
                }, 
                fail: function (fail) {
                  util.hideToast();
                  var fails = JSON.stringify(fail);
                  console.log("无保存到相册权限，请点击下方按钮重新授权:" + fails);
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 3000,
                    color: '#fff',
                    text: '无保存到相册权限，请点击下方按钮重新授权',
                    success: () => console.log('无保存到相册权限，请点击下方按钮重新授权')
                  })
                  that.setData({
                    showSetting: true
                  })
                }
              })
            }else{
              wx.saveImageToPhotosAlbum({
                filePath: path,
                success(result) {
                  util.hideToast();
                  console.log(result)
                  that.setData({
                    showRequestPopModalStatus: false,
                    showCodePopModalStatus: false
                  });
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: '保存成功',
                    success: () => console.log('保存成功')
                  })
                },
                fail: function (fail) {
                  util.hideToast();
                  var fails = JSON.stringify(fail);
                  console.log("保存失败:" + fails);
                  that.$wuxToast.show({
                    type: 'text',
                    timer: 1500,
                    color: '#fff',
                    text: '保存失败',
                    success: () => console.log('保存失败')
                  })
                }
              })
            }
          },fail: function (fail) {
            util.hideToast();
            var fails = JSON.stringify(fail);
            console.log("无法获取当前权限设置:" + fails);
            that.$wuxToast.show({
              type: 'text',
              timer: 1500,
              color: '#fff',
              text: '无法获取当前权限设置',
              success: () => console.log('无法获取当前权限设置')
            })
          }
        })
      },
      fail: function (err) {
        util.hideToast();
        var errs = JSON.stringify(err);
        console.log("获取图片信息失败:" + errs);
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '获取图片信息失败',
          success: () => console.log('获取图片信息失败')
        })
      }
    })
  }
})
