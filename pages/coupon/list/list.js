const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({

  /**
   * 页面的初始数据
   */
  data: {
    visual: 'hidden',
    tap: 4,
    // 我领取的亲友码
    getCouponList: [],
  },
  useThisCoupon:function(e){
    var index = e.currentTarget.dataset.index;
    app.globalData.couponItem = that.data.getCouponList[index];
    app.globalData.relativeCodeId = that.data.getCouponList[index].relativeCodeInfo.id;
    app.globalData.loadCouponListPage = true;
    console.log("我领取的优惠券列表页app.globalData.couponItem:" + index);
    wx.navigateBack({
      delta: 1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;

    that.getCouponList();

  },
  // 请求全部代金券
  getCouponList: function () {
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/getHoldeRelativeCodeInfo',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var results = res.data.data;
        util.hideToast();
        var stringifyresults = JSON.stringify(results);
        console.log("亲友码列表：" + results);
        that.setData({
          visual: results.length ? 'hidden' : 'show',
          getCouponList: results
        });
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    });
  },
  //不使用亲友码
  dontUseCouponFunc: function(){
    app.globalData.couponItem = -1;
    app.globalData.relativeCodeId = -1;
    app.globalData.loadCouponListPage = true;
    wx.navigateBack({
      delta: 1
    })
  }
})