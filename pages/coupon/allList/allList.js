const util = require("../../../utils/util.js");
var app = getApp();
var that;


Page({

  /**
   * 页面的初始数据
   */
  data: {
    visual: 'hidden',
    couponList: [],
    // 切换tab标记
    currentTabsIndex: 0,
    tap:1,
    // 我分享的亲友码
    shareCouponList: [],
    // 我领取的亲友码
    getCouponList: [],
  },
  //分享的亲友码进详情
  linkToCouponDetail: function (options) {
    var relativeCodeInfoId = options.currentTarget.id; 
    var status = options.currentTarget.dataset.status;
    if (status==1){
      return;
    }else{
      console.log("linkToCouponDetail-couponId:" + relativeCodeInfoId);
      console.log("linkToCouponDetail-status:" + status);
      wx.navigateTo({
        url: '/pages/coupon/detail/detail?relativeCodeInfoId=' + relativeCodeInfoId + '&status=' + status,
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;
    var tap = options.tap;
    if (tap == 4) {
      that.setData({
        tap: options.tap,
        currentTabsIndex: 1
      })
    }
    wx.setNavigationBarTitle({
      title: "我的亲友码"
    });

    that.getCouponList();

  },
  //分享
  onShareAppMessage: function (event) {
    console.log("event.from：" + event.from);
    console.log("onShareAppMessage-couponId:" + JSON.stringify(event));
    var couponId = event.target.id;
    var couponPrice = event.target.dataset.price;
    console.log("onShareAppMessage-couponId:" + couponId);
    if (event.from === 'button') {
      return {
        title: "福利快戳！亲友码买空气堡能减￥" + couponPrice.split("-")[1]+"!",
        path: '/pages/detail/friendCodeToBuy/detail?couponId=' + couponId + '&tap=1',
        imageUrl:'http://static.cakeboss.com.cn/kqbQinYouCodePostToNewUserN1.jpg',
        success: function (options) {
          util.shareSuccessTost();
        },
        fail: function (options) {
          util.shareCancelTost();
        }
      }
    }else{
      return {
        path: '/pages/index/index',
        imageUrl:'http://static.cakeboss.com.cn/kqbBanner1.svg',
        success: function (options) {
          // 转发成功
        },
        fail: function (options) {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: '取消分享'
          })
        }
      }
    }
  },
  //切换亲友码、拼团、积分商城
  onTabsItemTap: function (e) {
    var index = e.currentTarget.dataset.index;
    console.log(index);
    that.setData({
      currentTabsIndex: index
    })
    var dataFlag = false;
    if (index == 0) {
      that.setData({
        tap: 1  //我分享的
      })
      if (that.data.friendGoodsList == null || that.data.friendGoodsList == "") {
        dataFlag = true;
      }
    } else {
      that.setData({
        tap: 4  //我分享的
      })
      if (that.data.pointGoodsList == null || that.data.pointGoodsList == "") {
        dataFlag = true;
      }
    }
    if (dataFlag == true){
      that.getCouponList();
    }
  },
  // 请求全部代金券
  getCouponList: function () {
    //加载提示框
    util.showLoading();
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/getUserRelativeCodeInfoList',
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      data:{
        "type":that.data.tap
      },
      success: function (res) {
        var results = res.data;
        util.hideToast();
        console.log("代金券列表：" + JSON.stringify(results));

        that.setData({
          visual: results.length ? 'hidden' : 'show',
          couponList: results
        });

        if (that.data.tap == 1) {
          that.setData({
            shareCouponList: res.data
          })
        } else if (that.data.tap == 4) {
          that.setData({
            getCouponList: res.data
          })     
        }
      },
      fail: function () {
        util.hideToast();
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '请求失败',
          success: () => console.log('请求失败')
        })
      }
    });
  },
  //我领取的亲友码去使用
  useThisCoupon:function(e){
    var index = e.currentTarget.dataset.index;
    console.log("优惠券列表页我领取的优惠券app.globalData.couponItem的Index:" + index);
    app.globalData.couponItem = that.data.getCouponList[index];
    console.log("优惠券列表页我领取的优惠券app.globalData.couponItem:" + app.globalData.couponItem);
    app.globalData.relativeCodeId = that.data.getCouponList[index].relativeCodeInfo.id;
    app.globalData.loadCouponListPage = false;
    console.log("优惠券列表页我领取的优惠券app.globalData.couponItem的relativeCodeId:" + app.globalData.relativeCodeId);
    wx.switchTab({
      url: '/pages/index/index'
    })
    // wx.redirectTo({
    //   url: '/pages/detail/friendCodeToBuy/detail?tap=1&userInfo=self&goodsId='+ app.globalData.couponItem.goodsId
    // })
  }
})