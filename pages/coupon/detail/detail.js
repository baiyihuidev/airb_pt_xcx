const util = require("../../../utils/util.js");
var app = getApp();
var that;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    source:"",
    status:"",
    resultTime:""
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    this.$wuxToast = app.wux(this).$wuxToast;

    that.setData({
      status: options.status,
    })

    let relativeCodeInfoId = options.relativeCodeInfoId;
    wx.request({
      url: app.globalData.BaseURL + '/relativeCode/single/detail?id='+relativeCodeInfoId,
      method: 'GET',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "userViewId": app.globalData.userViewId
      },
      success: function (res) {
        var code = res.data.code;
        var msg = res.data.msg;
        var createTime = res.data.data.logs.createTime;
        if (code == 200) {
          that.setData({
            source:res.data.data,
          })
        } else {
          that.$wuxToast.show({
            type: 'text',
            timer: 1500,
            color: '#fff',
            text: msg,
            success: () => console.log(msg)
          })
        }
      },
      fail: function () {
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '数据请求失败',
          success: () => console.log('数据请求失败')
        })
      }
    })
  },
})