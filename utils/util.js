function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}


function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
  formatTime: formatTime
}

const app = getApp()


// 拼接POST请求参数
function json2Form(json) {
  var str = [];
  for (var p in json) {
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(json[p]));
  }
  return str.join("&");
}

//网络请求
function request(urlStr = "", parameters = "", success, method = "GET", header = {
  'content-type': 'application/json'
}) {
  wx.request({
    url: app.globalData.BaseURL + urlStr + (method == "GET" ? "?" : "") + parameters,
    data: { 'access_token': '1ab7019a2e844e96bf6a6667db23b55c' },
    method: method, // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    header: header ? header : "application/json", // 设置请求的 header
    success: function (res) {
      console.log(res);
      success(res);
    },
    fail: function () {
      // fail
      wx.hideToast();
    },
    complete: function () {
      // complete
      wx.hideToast();
    }
  })
}

//HUD 
//成功提示
function showSuccess(title = "成功啦", duration = 2500) {
  wx.showToast({
    title: title,
    icon: 'success',
    duration: (duration <= 0) ? 2500 : duration
  });
}
//loading提示
function showLoading(title = "请稍后", duration = 5000) {
  wx.showToast({
    title: title,
    icon: 'loading',
    mask: true,
    duration: (duration <= 0) ? 5000 : duration
  });
}
//隐藏提示框
function hideToast() {
  wx.hideToast();
}
// 分享成功提示
function shareSuccessTost() {
  wx.getSystemInfo({
    success: function (res) {
      console.log(res.platform)
      if (res.platform === "ios") {
        wx.showToast({
          title: '分享成功',
          duration: 1500,
          color: '#fff',
          icon: "none"
        })
      }
    }
  })
}
// 取消分享提示
function shareCancelTost() {
  wx.getSystemInfo({
    success: function (res) {
      console.log(res.platform)
      if (res.platform==="ios") {
        wx.showToast({
          title: '取消分享',
          duration: 1500,
          color: '#fff',
          icon:"none"
        })
      }
    }
  })
}

//显示带取消按钮的消息提示框
function alertViewWithCancel(title = "提示", content = "消息提示", confirm, showCancel = "true") {
  wx.showModal({
    title: title,
    content: content,
    showCancel: showCancel,
    confirmColor: '#00B0EB',
    success: function (res) {
      if (res.confirm) {
        confirm();
      }
    }
  });
}
//显示不取消按钮的消息提示框
function alertView(title = "提示", content = "消息提示", confirm) {
  alertViewWithCancel(title, content, confirm, false);
}

function checkOpenId(that) {
  /*
 * 若openId为空，则重新获取openId
 */
  if (app.globalData.openId == "") {
    // 微信登录，获取openId
    wx.login({
      success: function (wxRes) {
        console.log("登陆成功");
        console.log(wxRes);
        // 获取openid
        wx.request({
          url: app.globalData.BaseURL + '/miniprogram/wxxpay/openId' + app.globalData.accessToken,
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          data: { 'code': wxRes.code, 'xcxid': app.globalData.xcxidStr },
          success: function (res) {
            var openId = res.data.data;
            console.log("openId===" + openId);
            app.globalData.openId = openId;
          },
          fail: function (res) {
            console.log("失败");
            app.globalData.openId = "";
            that.$wuxToast.show({
              type: 'text',
              timer: 1500,
              color: '#fff',
              text: '获取微信登录失败，请退出重试',
              success: () => console.log('获取微信登录失败，请退出重试')
            })
          }
        })
      },
      fail: function () {
        console.log("登录失败");
        that.$wuxToast.show({
          type: 'text',
          timer: 1500,
          color: '#fff',
          text: '微信登录失败，请退出重试',
          success: () => console.log('微信登录失败，请退出重试')
        })
      }
    });
  }
}


var countdown = 60;
// 倒计时
function settime(that) {
  if (countdown == 0) {
    that.setData({
      codeStr: '重新获取验证码',
      codeCanTouch: true
    })
    countdown = 60;
    return;
  } else {
    that.setData({
      codeStr: countdown + 's后重新获取',
      codeCanTouch: false
    })

    countdown--;
  }
  setTimeout(function () {
    settime(that)
  }, 1000)
}

/* 手机号验证 */
function validatePhone(phone) {
  var mbTest = /^(1)[0-9]{10}$/;
  if (!mbTest.test(phone)) {
    return "请输入正确手机号";
  }
  return "";
}

//paraName 等找参数的名称
function GetUrlParam(url, paraName) {
  console.log('util的url=====' + url);
  console.log('util的paraName=====' + paraName);


  var arrObj = url.split("?");
  if (arrObj.length > 1) {
    var arrPara = arrObj[1].split("&");
    var arr;
    for (var i = 0; i < arrPara.length; i++) {
      arr = arrPara[i].split("=");
      if (arr != null && arr[0] == paraName) {
        return arr[1];
      }
    }
    return "";
  } else {
    return "";
  }
}

/**
 * 比较版本号
 */
function versionfunegt (ver1, ver2) {
  var version1pre = parseFloat(ver1);
  var version2pre = parseFloat(ver2);
  var version1next = ver1.replace(version1pre + ".", "");
  var version2next = ver2.replace(version2pre + ".", "");
  if (version1pre > version2pre) {
    return true;
  } else if (version1pre < version2pre) {
    return false;
  } else {
    if (version1next >= version2next) {
      return true;
    } else {
      return false;
    }
  }
}

module.exports = {
  formatTime: formatTime,
  json2Form: json2Form,
  request: request,
  showSuccess: showSuccess,
  showLoading: showLoading,
  hideToast: hideToast,
  alertViewWithCancel: alertViewWithCancel,
  alertView: alertView,
  checkOpenId: checkOpenId,
  settime: settime,
  validatePhone: validatePhone,
  GetUrlParam: GetUrlParam,
  versionfunegt: versionfunegt,
  shareSuccessTost: shareSuccessTost,
  shareCancelTost: shareCancelTost,
}

module.exports = {
  formatTime: formatTime,
  json2Form: json2Form,
  request: request,
  showSuccess: showSuccess,
  showLoading: showLoading,
  hideToast: hideToast,
  alertViewWithCancel: alertViewWithCancel,
  alertView: alertView,
  checkOpenId: checkOpenId,
  settime: settime,
  validatePhone: validatePhone,
  GetUrlParam: GetUrlParam,
  versionfunegt: versionfunegt,
  shareSuccessTost: shareSuccessTost,
  shareCancelTost: shareCancelTost,
}


